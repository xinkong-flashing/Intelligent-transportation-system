# 智能交通系统

#### 介绍
Android智能交通APP平台系统基于Android MVC模式开发，由移动端APP（V）、业务逻辑层（C）和数据持久化层（M）三层结构组成。Android 智能交通 APP 平台系统共分为两个部分，分别是移动端 APP、后台平台管理系统。
移动端APP由我的账户、账户管理、车辆充值、个人中心、数据分析、生活助手等六个部分组成。

#### 软件架构
软件架构说明


#### 安装教程

1.  这是一个基于android的软件，要想使用软件，必须具有android studio 开发软件
2.  这是一个android与客户端的交互软件，是要软件时，必须开启网络

#### 使用说明

1.  这是一个android的iPad布局项目，建议首先使用ipad模拟器


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
