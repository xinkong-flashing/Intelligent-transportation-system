var data = new Array;
var time = new Array;
function setData(Array){
    try {
        tmpstr = JSON.stringify(Array);
        substr = tmpstr.substring(2,tmpstr.length-2);
        data = substr.split(',');
        data.reverse();
    }catch(exception) {
        alert(exception);
    }
} 
function setTime(Array){
    try {
        tmpstr = JSON.stringify(Array);
        substr = tmpstr.substring(2,tmpstr.length-2);
        time = substr.split(',');
        time.reverse();
    }catch(exception) {
        alert(exception);
    }
} 

window.onload = function draw() {
    //初始化
    var mychart = echarts.init(this.document.getElementById("echarts"));
    this.setInterval(function () {
        var option = {
            xAxis: {
                type: 'category',
                data: time
            },
            yAxis: {
                type: 'value'
            },
            series: [{
                data: data,
                type: 'line'
            }]
        };
        //设置数据
        mychart.setOption(option);
        
    }, 3000)
    
}