package com.lenovo.smarttraffic.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class AccountManagementDatabase extends SQLiteOpenHelper {

    private Context context;
    public static final String DATABASE = "create table Account(" +
            "id integer primary key autoincrement," +
            "carnumber text," +
            "recharge integer," +
            "balance integer," +
            "people text," +
            "time text)";

    public AccountManagementDatabase(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
