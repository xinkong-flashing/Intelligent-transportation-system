package com.lenovo.smarttraffic.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class EnvironmentDatabase extends SQLiteOpenHelper {

    private Context context;
    private static final String CREATE_INDEX = "create table Target ("
            + "id integer primary key autoincrement,"
            + "temperature integer,"
            + "humidity integer,"
            + "illumination integer,"
            + "co2 integer,"
            + "pm integer,"
            + "way integer,"
            + "time text)";

    public EnvironmentDatabase(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_INDEX);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
