package com.lenovo.smarttraffic.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class CarErrorResultDatabase extends SQLiteOpenHelper {

    public static final String CAR_ERRPR_LEFT = "Create table CarErrorLeft(" +
            "id integer primary key autoincrement," +
            "carnumber text," +
            "number integer," +
            "marks integer," +
            "penalty integer)";
    public static final String CAR_ERROE_RIGHT = "Create table CarErrorRight(" +
            "id integer primary key autoincrement," +
            "carnumber text," +
            "time text," +
            "address text," +
            "remark text," +
            "marks integer," +
            "penalty integer)";
    private Context context;

    public CarErrorResultDatabase(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CAR_ERRPR_LEFT);
        sqLiteDatabase.execSQL(CAR_ERROE_RIGHT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
