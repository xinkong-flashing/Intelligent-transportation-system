package com.lenovo.smarttraffic.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class LifeHelperDatabase extends SQLiteOpenHelper {

    private Context context;
    public static final String DATA = "create table AllWeather(" +
            "id integer primary key autoincrement," +
            "pm25 integer," +
            "temperature integer," +
            "humidity integer," +
            "co2 integer," +
            "time text)";

    public LifeHelperDatabase(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
