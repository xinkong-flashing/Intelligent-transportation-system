package com.lenovo.smarttraffic.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.widget.Toast;

public class AccountDatabase extends SQLiteOpenHelper {

    public static final String CREATE_ACCOUNT = "create table Account("
            +"id integer primary key autoincrement,"
            +"car integer,"
            +"recharge integer,"
            +"admin text,"
            +"time text)";
    public static final String BALANCE_ACCOUNT = "create table Balance("
            +"id integer primary key autoincrement,"
            +"car integer,"
            +"balance integer)";
    private Context context;

    public AccountDatabase(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_ACCOUNT);
        sqLiteDatabase.execSQL(BALANCE_ACCOUNT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
