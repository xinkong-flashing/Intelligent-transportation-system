package com.lenovo.smarttraffic.database.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import com.lenovo.smarttraffic.ui.fragment.RechargeRecordFragment;

public class CarAccountDataBase extends SQLiteOpenHelper {

    public static final String CREAT_CAR = "create table CarAccount("
            +"id integer primary key autoincrement,"
            +"plate text,"
            +"money integer,"
            +"balance integer,"
            +"people text,"
            +"time text)";
    private Context context;

    public CarAccountDataBase(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREAT_CAR);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
