package com.lenovo.smarttraffic.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;

import org.w3c.dom.Text;

public class BusInquiryDialog extends Dialog {

    private Context context;    //上下文对象
    private int themeResId; //布局文件id
    private ListView listView;
    private MyBaseAdapter adapter;
    private TextView textView;
    private int all = 0;

    public BusInquiryDialog(Context context, int themeResId) {
        super(context, R.style.MyDialog);
        this.context = context;
        this.themeResId = themeResId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //提前设置一样Dialog样式
        Window dialogWindow = getWindow();
        dialogWindow.setGravity(Gravity.CENTER);    //设置dialog居中
        setContentView(themeResId);
        WindowManager windowManager = ((Activity)context).getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = display.getWidth()*1/2;  //设置dialog宽度为屏幕的1/2
        lp.height = display.getHeight()*4/5;
        getWindow().setAttributes(lp);
        setCanceledOnTouchOutside(false);   //点击外部dialog不消失

        textView = findViewById(R.id.all);

        //创建listView
        listView = findViewById(R.id.bus_inquiry_dialog_listview);
        adapter = new MyBaseAdapter();
        listView.setAdapter(adapter);

        handler.sendEmptyMessage(0x001);

        Button button = findViewById(R.id.bus_inquiry_dialog_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    //handler3秒刷新数据
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0x001) {
                listView.setAdapter(adapter);
                handler.sendEmptyMessageDelayed(0x001, 3000);
            }
        }
    };

    //创建ListView适配器
    private class MyBaseAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return 16;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view;
            ViewHolder viewHolder;
            if (convertView == null) {
                view = LayoutInflater.from(context).inflate(R.layout.bus_inquiry_dialog_list_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.tv1 = view.findViewById(R.id.bus_inquiry_dialog_item1);
                viewHolder.tv2 = view.findViewById(R.id.bus_inquiry_dialog_item2);
                viewHolder.tv3 = view.findViewById(R.id.bus_inquiry_dialog_item3);
                view.setTag(viewHolder);
            } else {
                view = convertView;
                viewHolder = (ViewHolder) view.getTag();
            }
            if (position == 0) {
                viewHolder.tv1.setText("序号");
                viewHolder.tv2.setText("公交车编号");
                viewHolder.tv3.setText("承载人数");
            } else {
                int i = (int)(Math.random()*100);
                viewHolder.tv1.setText(position+"");
                viewHolder.tv2.setText(position+"");
                viewHolder.tv3.setText(i + "");
                all = all + i;
            }
            textView.setText(all+"");
            return view;
        }
        class ViewHolder{
            TextView tv1;
            TextView tv2;
            TextView tv3;
        }
    }


}
