package com.lenovo.smarttraffic.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.database.dao.CarAccountDataBase;
import com.lenovo.smarttraffic.ui.activity.CarAccountActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyCarDialog extends Dialog implements View.OnClickListener {

    private Context context;    //上下文
    private int layoutResID;    //布局文件id
    private String[] data;  //车牌号数据
    private String plate = "";   //车牌号字符串
    private TextView tvPlate;
    private CarAccountDataBase carAccountDataBase;
    private EditText editText;
    private int[] balance;
    private CheckBox oneCheckBox;
    private CheckBox twoCheckBox;
    private CheckBox threeCheckBox;
    private CheckBox foreCheckBox;
    private TextView textView;
    private TextView textView1;
    private TextView textView2;
    private TextView textView3;

    //在构造方法中提前加载了dialog样式
    public MyCarDialog(Context context, int layoutResID, String[] data, int[] balance) {
        super(context, R.style.MyDialog);   //加载Dialog的样式
        this.context = context;
        this.layoutResID = layoutResID;
        this.data = data;
        this.balance = balance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //提前设置一些Dialog样式
        Window dialogWindow = getWindow();
        dialogWindow.setGravity(Gravity.CENTER);    //设置dialog显示居中
        setContentView(layoutResID);
        WindowManager windowManager = ((Activity) context).getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = display.getWidth()*1/2;  //设置display的宽度为屏幕的1/2
        getWindow().setAttributes(lp);
        setCanceledOnTouchOutside(false);   //点击外部Dialog不消失

        //注册按钮
        Button btnOne = findViewById(R.id.car_dialog_btn1);
        Button btnTwo = findViewById(R.id.car_dialog_btn2);
        btnOne.setOnClickListener(this);
        btnTwo.setOnClickListener(this);

        //显示车牌号
        tvPlate = findViewById(R.id.car_dialog_plate);
        for (int i = 0; i < data.length; i++) {
            plate = plate + data[i] + " ";
        }
        tvPlate.setText(plate);

        //获取EditView控件
        editText = findViewById(R.id.car_account_et);

        oneCheckBox = ((Activity) context).findViewById(R.id.car_account_one_cb);
        twoCheckBox = ((Activity) context).findViewById(R.id.car_account_two_cb);
        threeCheckBox = ((Activity) context).findViewById(R.id.car_account_three_cb);
        foreCheckBox = ((Activity) context).findViewById(R.id.car_account_fore_cb);

        textView = ((Activity) context).findViewById(R.id.car_account_one_tv_balance);
        textView1 = ((Activity) context).findViewById(R.id.car_account_two_tv_balance);
        textView2 = ((Activity) context).findViewById(R.id.car_account_three_tv_balance);
        textView3 = ((Activity) context).findViewById(R.id.car_account_fore_tv_balance);

        //判断网络状态信息
        if (getNetworkState()) {
            Toast.makeText(context, "当前网络可用！", Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(context, "当前网络不可用！", Toast.LENGTH_SHORT).show();
        }

    }

    //获取网络状态
    private boolean getNetworkState() {
        //获取手机所有的连接对象（包括WiFi、net等连接的管理）
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //获取NetworkInfo对象
        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
        //遍历每一个对象
        for (NetworkInfo networkInfo : networkInfos) {
            if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                //网络状态可用
                return true;
            }
        }
        //没有可用的网络
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.car_dialog_btn1:
                setData();
                Toast.makeText(context, "充值成功！", Toast.LENGTH_SHORT).show();
                if (oneCheckBox.isChecked()) {
                    textView.setText(((Integer.parseInt(textView.getText().toString())) + (Integer.parseInt(editText.getText().toString()))) + "");
                }
                if (twoCheckBox.isChecked()) {
                    textView1.setText(((Integer.parseInt(textView1.getText().toString())) + (Integer.parseInt(editText.getText().toString()))) + "");
                }
                if (threeCheckBox.isChecked()) {
                    textView2.setText(((Integer.parseInt(textView2.getText().toString())) + (Integer.parseInt(editText.getText().toString()))) + "");
                }
                if (foreCheckBox.isChecked()) {
                    textView3.setText(((Integer.parseInt(textView3.getText().toString())) + (Integer.parseInt(editText.getText().toString()))) + "");
                }
                if (data.length == 1) {
                    if (data[0].equals("辽A10001")) {
                        Log.d("text", data[0]);
                        textView.setText(((Integer.parseInt(textView.getText().toString())) + (Integer.parseInt(editText.getText().toString()))) + "");
                    } else if (data[0].equals("辽A10002")) {
                        textView1.setText(((Integer.parseInt(textView1.getText().toString())) + (Integer.parseInt(editText.getText().toString()))) + "");
                    } else if (data[0].equals("辽A10003")) {
                        textView2.setText(((Integer.parseInt(textView2.getText().toString())) + (Integer.parseInt(editText.getText().toString()))) + "");
                    } else if (data[0].equals("辽A10004")) {
                        textView3.setText(((Integer.parseInt(textView3.getText().toString())) + (Integer.parseInt(editText.getText().toString()))) + "");
                    }
                }
                dismiss();
                break;
            case R.id.car_dialog_btn2:
                dismiss();
                Toast.makeText(context, "取消充值！", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    //创建数据库，存储数据
    public void setData() {
        //创建数据库
        carAccountDataBase = new CarAccountDataBase(context, "CarAccount.db", null, 1);
        carAccountDataBase.getWritableDatabase();
        //存储数据
        SQLiteDatabase db = carAccountDataBase.getWritableDatabase();
        ContentValues values = new ContentValues();
        for (int i = 0; i < data.length; i++) {
            values.put("plate", data[i]);
            values.put("money", Integer.parseInt(editText.getText().toString()));
            values.put("balance", balance[i] + Integer.parseInt(editText.getText().toString()));
            values.put("people", "admin");
            //获取时间
            SimpleDateFormat df = new SimpleDateFormat("YYYY年MM月dd日 HH:mm:ss");
            String time = df.format(new Date());
            values.put("time", time);
            db.insert("CarAccount", null, values);
        }
    }

}
