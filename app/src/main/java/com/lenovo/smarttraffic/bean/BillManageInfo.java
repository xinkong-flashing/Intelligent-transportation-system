package com.lenovo.smarttraffic.bean;

public class BillManageInfo {

    private int car;
    private int money;
    private String people;
    private String time;

    public BillManageInfo() {
        super();
    }

    public BillManageInfo(int car, int money, String people, String time) {
        super();
        this.car = car;
        this.money = money;
        this.people = people;
        this.time = time;
    }

    public void setCar(int car) {
        this.car = car;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getCar() {
        return car;
    }

    public int getMoney() {
        return money;
    }

    public String getPeople() {
        return people;
    }

    public String getTime() {
        return time;
    }
}
