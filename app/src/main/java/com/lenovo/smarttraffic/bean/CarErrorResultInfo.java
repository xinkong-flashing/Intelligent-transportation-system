package com.lenovo.smarttraffic.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarErrorResultInfo {

    private String ERRMSG;
    @SerializedName("ROWS_DETAIL")
    private List<Data1> Data1;
    private String RESULT;

    public CarErrorResultInfo(String ERRMSG, List<com.lenovo.smarttraffic.bean.Data1> data1, String RESULT) {
        this.ERRMSG = ERRMSG;
        Data1 = data1;
        this.RESULT = RESULT;
    }

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }

    public void setData1(List<com.lenovo.smarttraffic.bean.Data1> data1) {
        Data1 = data1;
    }

    public void setRESULT(String RESULT) {
        this.RESULT = RESULT;
    }

    public String getERRMSG() {
        return ERRMSG;
    }

    public List<com.lenovo.smarttraffic.bean.Data1> getData1() {
        return Data1;
    }

    public String getRESULT() {
        return RESULT;
    }
}
