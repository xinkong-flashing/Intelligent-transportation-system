package com.lenovo.smarttraffic.bean.LifeHelper;

import com.google.gson.annotations.SerializedName;

public class AllWeatherInfo {

    @SerializedName("pm2.5")
    private int pm25;
    private int co2;
    private int LightIntensity;
    private int humidity;
    private int temperature;

    public void setPm25(int pm25) {
        this.pm25 = pm25;
    }

    public void setCo2(int co2) {
        this.co2 = co2;
    }

    public void setLightIntensity(int lightIntensity) {
        LightIntensity = lightIntensity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getPm25() {
        return pm25;
    }

    public int getCo2() {
        return co2;
    }

    public int getLightIntensity() {
        return LightIntensity;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getTemperature() {
        return temperature;
    }
}
