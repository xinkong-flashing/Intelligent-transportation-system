package com.lenovo.smarttraffic.bean;

public class LeftData {

    private String carnumber;
    private int number;
    private int marks;
    private int penalty;

    public LeftData() {
    }

    public LeftData(String carnumber, int number, int marks, int penalty) {
        this.carnumber = carnumber;
        this.number = number;
        this.marks = marks;
        this.penalty = penalty;
    }

    public void setCarnumber(String carnumber) {
        this.carnumber = carnumber;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public String getCarnumber() {
        return carnumber;
    }

    public int getNumber() {
        return number;
    }

    public int getMarks() {
        return marks;
    }

    public int getPenalty() {
        return penalty;
    }
}
