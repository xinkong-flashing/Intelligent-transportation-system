package com.lenovo.smarttraffic.bean;

public class ROWS_DETAIL {

    private int id;
    private String carnumber;
    private String pcode;
    private String pStringtime;
    private String paddr;

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setCarnumber(String carnumber) {
        this.carnumber = carnumber;
    }
    public String getCarnumber() {
        return carnumber;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }
    public String getPcode() {
        return pcode;
    }

    public void setPStringtime(String pStringtime) {
        this.pStringtime = pStringtime;
    }
    public String getPStringtime() {
        return pStringtime;
    }

    public void setPaddr(String paddr) {
        this.paddr = paddr;
    }
    public String getPaddr() {
        return paddr;
    }

}
