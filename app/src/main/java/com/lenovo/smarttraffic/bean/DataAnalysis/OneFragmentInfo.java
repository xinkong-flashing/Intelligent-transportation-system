package com.lenovo.smarttraffic.bean.DataAnalysis;

import java.util.List;

public class OneFragmentInfo {

    private List<ROWS_DETAIL> ROWS_DETAIL;

    public void setROWS_DETAIL(List<OneFragmentInfo.ROWS_DETAIL> ROWS_DETAIL) {
        this.ROWS_DETAIL = ROWS_DETAIL;
    }

    public List<OneFragmentInfo.ROWS_DETAIL> getROWS_DETAIL() {
        return ROWS_DETAIL;
    }

    public class ROWS_DETAIL{
        private String carnumber;

        public void setCarnumber(String carnumber) {
            this.carnumber = carnumber;
        }

        public String getCarnumber() {
            return carnumber;
        }
    }
}
