package com.lenovo.smarttraffic.bean.LifeHelper;

import java.util.List;

public class TemperatureInfo {

    private int WCurrent;
    List<ROWS_DETAIL> ROWS_DETAIL;

    public void setWCurrent(int WCurrent) {
        this.WCurrent = WCurrent;
    }

    public List<TemperatureInfo.ROWS_DETAIL> getROWS_DETAIL() {
        return ROWS_DETAIL;
    }

    public int getWCurrent() {
        return WCurrent;
    }

    public void setROWS_DETAIL(List<TemperatureInfo.ROWS_DETAIL> ROWS_DETAIL) {
        this.ROWS_DETAIL = ROWS_DETAIL;
    }

    public class ROWS_DETAIL{

        private String temperature;
        private String WData;
        private String type;

        public void setTemperature(String temperature) {
            this.temperature = temperature;
        }

        public void setWData(String WData) {
            this.WData = WData;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTemperature() {
            return temperature;
        }

        public String getWData() {
            return WData;
        }

        public String getType() {
            return type;
        }
    }

}
