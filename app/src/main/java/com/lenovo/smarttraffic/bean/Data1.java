package com.lenovo.smarttraffic.bean;

public class Data1 {

    private int id;
    private String carnumber;
    private String pcode;
    private String pdatetime;
    private String paddr;

    public Data1(int id, String carnumber, String pcode, String pdatetime, String paddr) {
        this.id = id;
        this.carnumber = carnumber;
        this.pcode = pcode;
        this.pdatetime = pdatetime;
        this.paddr = paddr;
    }

    public int getId() {
        return id;
    }

    public String getCarnumber() {
        return carnumber;
    }

    public String getPcode() {
        return pcode;
    }

    public String getPdatetime() {
        return pdatetime;
    }

    public String getPaddr() {
        return paddr;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCarnumber(String carnumber) {
        this.carnumber = carnumber;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public void setPdatetime(String pdatetime) {
        this.pdatetime = pdatetime;
    }

    public void setPaddr(String paddr) {
        this.paddr = paddr;
    }
}
