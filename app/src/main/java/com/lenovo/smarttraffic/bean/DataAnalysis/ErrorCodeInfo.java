package com.lenovo.smarttraffic.bean.DataAnalysis;

import java.util.List;

public class ErrorCodeInfo {

    private String ERRMSG;
    private List<ROWS_DETAIL> ROWS_DETAIL;
    private String RESULT;

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }

    public void setROWS_DETAIL(List<ErrorCodeInfo.ROWS_DETAIL> ROWS_DETAIL) {
        this.ROWS_DETAIL = ROWS_DETAIL;
    }

    public void setRESULT(String RESULT) {
        this.RESULT = RESULT;
    }

    public String getERRMSG() {
        return ERRMSG;
    }

    public List<ErrorCodeInfo.ROWS_DETAIL> getROWS_DETAIL() {
        return ROWS_DETAIL;
    }

    public String getRESULT() {
        return RESULT;
    }

    public class ROWS_DETAIL{

        private String pcode;
        private String premarks;
        private int pmoney;
        private int pscore;

        public void setPcode(String pcode) {
            this.pcode = pcode;
        }

        public void setPremarks(String premarks) {
            this.premarks = premarks;
        }

        public void setPmoney(int pmoney) {
            this.pmoney = pmoney;
        }

        public void setPscore(int pscore) {
            this.pscore = pscore;
        }

        public String getPcode() {
            return pcode;
        }

        public String getPremarks() {
            return premarks;
        }

        public int getPmoney() {
            return pmoney;
        }

        public int getPscore() {
            return pscore;
        }
    }

}
