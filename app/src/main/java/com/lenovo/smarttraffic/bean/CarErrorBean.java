package com.lenovo.smarttraffic.bean;

import java.util.List;

public class CarErrorBean {

    private String ERRMSG;
    private List<ROWS_DETAIL> ROWS_DETAIL;
    private String RESULT;

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }
    public String getERRMSG() {
        return ERRMSG;
    }

    public void setROWS_DETAIL(List<ROWS_DETAIL> ROWS_DETAIL) {
        this.ROWS_DETAIL = ROWS_DETAIL;
    }
    public List<ROWS_DETAIL> getROWS_DETAIL() {
        return ROWS_DETAIL;
    }

    public void setRESULT(String RESULT) {
        this.RESULT = RESULT;
    }
    public String getRESULT() {
        return RESULT;
    }

}
