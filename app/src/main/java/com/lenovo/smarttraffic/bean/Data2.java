package com.lenovo.smarttraffic.bean;

public class Data2 {

    private String pcode;
    private String premarks;
    private int pmoney;
    private int pscore;

    public Data2(String pcode, String premarks, int pmoney, int pscore) {
        this.pcode = pcode;
        this.premarks = premarks;
        this.pmoney = pmoney;
        this.pscore = pscore;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public void setPremarks(String premarks) {
        this.premarks = premarks;
    }

    public void setPmoney(int pmoney) {
        this.pmoney = pmoney;
    }

    public void setPscore(int pscore) {
        this.pscore = pscore;
    }

    public String getPcode() {
        return pcode;
    }

    public String getPremarks() {
        return premarks;
    }

    public int getPmoney() {
        return pmoney;
    }

    public int getPscore() {
        return pscore;
    }
}
