package com.lenovo.smarttraffic.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CarErrorResultInfo2 {

    private String ERRMSG;
    @SerializedName("ROWS_DETAIL")
    private List<Data2> Data2;
    private String RESULT;

    public CarErrorResultInfo2(String ERRMSG, List<com.lenovo.smarttraffic.bean.Data2> data2, String RESULT) {
        this.ERRMSG = ERRMSG;
        Data2 = data2;
        this.RESULT = RESULT;
    }

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }

    public void setData2(List<com.lenovo.smarttraffic.bean.Data2> data2) {
        Data2 = data2;
    }

    public void setRESULT(String RESULT) {
        this.RESULT = RESULT;
    }

    public String getERRMSG() {
        return ERRMSG;
    }

    public List<com.lenovo.smarttraffic.bean.Data2> getData2() {
        return Data2;
    }

    public String getRESULT() {
        return RESULT;
    }
}
