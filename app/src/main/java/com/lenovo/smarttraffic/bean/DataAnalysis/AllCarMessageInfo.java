package com.lenovo.smarttraffic.bean.DataAnalysis;

import java.util.List;

public class AllCarMessageInfo {

    private String ERRMSG;
    private List<ROWS_DETAIL> ROWS_DETAIL;
    private String RESULT;

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }

    public void setROWS_DETAIL(List<AllCarMessageInfo.ROWS_DETAIL> ROWS_DETAIL) {
        this.ROWS_DETAIL = ROWS_DETAIL;
    }

    public void setRESULT(String RESULT) {
        this.RESULT = RESULT;
    }

    public String getERRMSG() {
        return ERRMSG;
    }

    public List<AllCarMessageInfo.ROWS_DETAIL> getROWS_DETAIL() {
        return ROWS_DETAIL;
    }

    public String getRESULT() {
        return RESULT;
    }

    public class ROWS_DETAIL{

        private String carnumber;
        private int number;
        private String pcardid;
        private String carbrand;
        private String buydate;

        public void setCarnumber(String carnumber) {
            this.carnumber = carnumber;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public void setPcardid(String pcardid) {
            this.pcardid = pcardid;
        }

        public void setCarbrand(String carbrand) {
            this.carbrand = carbrand;
        }

        public void setBuydate(String buydate) {
            this.buydate = buydate;
        }

        public String getCarnumber() {
            return carnumber;
        }

        public int getNumber() {
            return number;
        }

        public String getPcardid() {
            return pcardid;
        }

        public String getCarbrand() {
            return carbrand;
        }

        public String getBuydate() {
            return buydate;
        }
    }

}
