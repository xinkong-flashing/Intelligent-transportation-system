package com.lenovo.smarttraffic.bean;

public class TrafficLightInfo {

    private int cross;
    private int red;
    private int yellow;
    private int green;

    public TrafficLightInfo() {
        super();
    }

    public TrafficLightInfo(int cross, int red, int yellow, int green) {
        super();
        this.cross = cross;
        this.red = red;
        this.yellow = yellow;
        this.green = green;
    }

    public void setCross(int cross) {
        this.cross = cross;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public void setYellow(int yellow) {
        this.yellow = yellow;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getCross() {
        return cross;
    }

    public int getRed() {
        return red;
    }

    public int getYellow() {
        return yellow;
    }

    public int getGreen() {
        return green;
    }
}
