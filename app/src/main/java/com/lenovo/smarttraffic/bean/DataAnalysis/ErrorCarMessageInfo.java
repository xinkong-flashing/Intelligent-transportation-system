package com.lenovo.smarttraffic.bean.DataAnalysis;

import java.util.List;

public class ErrorCarMessageInfo {

    private String ERRMSG;
    private List<ROWS_DETAIL> ROWS_DETAIL;
    private String RESULT;

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }

    public void setROWS_DETAIL(List<ErrorCarMessageInfo.ROWS_DETAIL> ROWS_DETAIL) {
        this.ROWS_DETAIL = ROWS_DETAIL;
    }

    public void setRESULT(String RESULT) {
        this.RESULT = RESULT;
    }

    public String getERRMSG() {
        return ERRMSG;
    }

    public List<ErrorCarMessageInfo.ROWS_DETAIL> getROWS_DETAIL() {
        return ROWS_DETAIL;
    }

    public String getRESULT() {
        return RESULT;
    }

    public class ROWS_DETAIL{

        private int id;
        private String carnumber;
        private String pcode;
        private String pdatetime;
        private String paddr;

        public void setId(int id) {
            this.id = id;
        }

        public void setCarnumber(String carnumber) {
            this.carnumber = carnumber;
        }

        public void setPcode(String pcode) {
            this.pcode = pcode;
        }

        public void setPdatetime(String pdatetime) {
            this.pdatetime = pdatetime;
        }

        public void setPaddr(String paddr) {
            this.paddr = paddr;
        }

        public int getId() {
            return id;
        }

        public String getCarnumber() {
            return carnumber;
        }

        public String getPcode() {
            return pcode;
        }

        public String getPdatetime() {
            return pdatetime;
        }

        public String getPaddr() {
            return paddr;
        }
    }

}
