package com.lenovo.smarttraffic.bean.DataAnalysis;

public class RemarksCodeInfo {

    private String remarks;
    private int number;

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getRemarks() {
        return remarks;
    }

    public int getNumber() {
        return number;
    }
}
