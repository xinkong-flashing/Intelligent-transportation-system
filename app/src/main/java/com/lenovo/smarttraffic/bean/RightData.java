package com.lenovo.smarttraffic.bean;

public class RightData {

    private String carnumber;
    private String time;
    private String address;
    private String remark;
    private int marks;
    private int penalty;

    public RightData() {
    }

    public RightData(String carnumber, String time, String address, String remark, int marks, int penalty) {
        this.carnumber = carnumber;
        this.time = time;
        this.address = address;
        this.remark = remark;
        this.marks = marks;
        this.penalty = penalty;
    }

    public void setTimes(String time) {
        this.time = time;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public void setCarnumber(String carnumber) {
        this.carnumber = carnumber;
    }

    public String getCarnumber() {
        return carnumber;
    }

    public String getTimes() {
        return time;
    }

    public String getAddress() {
        return address;
    }

    public String getRemark() {
        return remark;
    }

    public int getMarks() {
        return marks;
    }

    public int getPenalty() {
        return penalty;
    }
}
