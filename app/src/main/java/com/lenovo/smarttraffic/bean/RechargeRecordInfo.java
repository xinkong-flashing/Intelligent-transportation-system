package com.lenovo.smarttraffic.bean;

public class RechargeRecordInfo {

    private String plate;
    private int money;
    private int balance;
    private String people;
    private String time;

    public RechargeRecordInfo() {
        super();
    }

    public RechargeRecordInfo(String plate, int money, int balance, String people, String time) {
        this.plate = plate;
        this.money = money;
        this.balance = balance;
        this.people = people;
        this.time = time;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPlate() {
        return plate;
    }

    public int getMoney() {
        return money;
    }

    public int getBalance() {
        return balance;
    }

    public String getPeople() {
        return people;
    }

    public String getTime() {
        return time;
    }
}
