package com.lenovo.smarttraffic.bean;

public class EnvironmentIndexInfo {

    private int temperature;
    private int humidity;
    private int illumination;
    private int co2;
    private int pm;
    private int way;

    public EnvironmentIndexInfo() {
        super();
    }

    public EnvironmentIndexInfo(int temperature, int humidity, int illumination, int co2, int pm, int way) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.illumination = illumination;
        this.co2 = co2;
        this.pm = pm;
        this.way = way;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public void setIllumination(int illumination) {
        this.illumination = illumination;
    }

    public void setCo2(int co2) {
        this.co2 = co2;
    }

    public void setPm(int pm) {
        this.pm = pm;
    }

    public void setWay(int way) {
        this.way = way;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getIllumination() {
        return illumination;
    }

    public int getCo2() {
        return co2;
    }

    public int getPm() {
        return pm;
    }

    public int getWay() {
        return way;
    }
}
