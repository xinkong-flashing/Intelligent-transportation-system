package com.lenovo.smarttraffic.ui.fragment.LifeHelper;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.database.dao.LifeHelperDatabase;

import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class Co2Fragment extends Fragment {


    private View view;
    private LifeHelperDatabase lifeHelperDatabase;
    private WebView webView;

    public Co2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_co2, container, false);
        //创建数据库
        lifeHelperDatabase = new LifeHelperDatabase(getContext(), "LifeHelper.db", null, 1);
        lifeHelperDatabase.getWritableDatabase();
        //初始化webview
        webView = view.findViewById(R.id.life_helper_co2_fragment_webview);
        webView.loadUrl("file:///android_asset/LifeHelper/LineGraph.html");
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        //handler每3秒发送一次消息
        handler.sendEmptyMessage(0x014);
        return view;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0x014) {
                int n = 0;
                int[] data = new int[20];
                String[] time = new String[20];
                SQLiteDatabase db = lifeHelperDatabase.getWritableDatabase();
                Cursor cursor = db.query("AllWeather", null, null, null, null, null, null);
                if (cursor.moveToFirst()) {
                    do {
                        data[n] = cursor.getInt(cursor.getColumnIndex("co2"));
                        time[n] = cursor.getString(cursor.getColumnIndex("time"));
                        n++;
                    }while (cursor.moveToNext());
                }
                cursor.close();
                //获取这组数据的最大co2浓度
                int maxData = 0;
                for (int i = 0; i < data.length; i++) {
                    if (data[i] > maxData) {
                        maxData = data[i];
                    }
                }
                TextView textView = view.findViewById(R.id.life_helper_co2_fragment_textview);
                textView.setText("过去1分钟最大相对浓度："+maxData);
                //传递数组值给web
                webView.loadUrl("javascript:getData(\""+ Arrays.toString(time) +"\", \""+Arrays.toString(data)+"\")");
            }
            handler.sendEmptyMessageDelayed(0x014, 3000);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
}
