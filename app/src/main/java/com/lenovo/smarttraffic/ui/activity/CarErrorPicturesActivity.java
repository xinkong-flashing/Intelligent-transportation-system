package com.lenovo.smarttraffic.ui.activity;

import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.lenovo.smarttraffic.R;

public class CarErrorPicturesActivity extends BaseActivity {

    private ImageView imageView;
    /** 记录是拖拉照片模式还是放大缩小照片模式 */
    private int mode = 0;// 初始状态
    /** 拖拉照片模式 */
    private static final int MODE_DRAG = 1;
    /** 放大缩小照片模式 */
    private static final int MODE_ZOOM = 2;

    /** 用于记录开始时候的坐标位置 */
    private PointF startPoint = new PointF();
    /** 用于记录拖拉图片移动的坐标位置 */
    private Matrix matrix = new Matrix();
    /** 用于记录图片要进行拖拉时候的坐标位置 */
    private Matrix currentMatrix = new Matrix();

    /** 两个手指的开始距离 */
    private float startDis;
    /** 两个手指的中间点 */
    private PointF midPoint;
    private Button button;
    private int [] ints = new int[] { R.mipmap.weizhang01,R.mipmap.weizhang02,R.mipmap.weizhang03,R.mipmap.weizhang04};
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolBar(findViewById(R.id.toolbar), true, "违章抓拍");
        imageView = findViewById(R.id.image_scale);
        button = findViewById(R.id.btn_fanhui2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        chuMo();
    }

    public void chuMo(){

        imageView.setImageResource(ints[CarErrorPictureActivity.position]);
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()&MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_DOWN:
                        mode=MODE_DRAG;
                        currentMatrix.set(imageView.getImageMatrix());
                        startPoint.set(event.getX(),event.getY());
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mode==MODE_DRAG){
                            float dx=event.getX()-startPoint.x;
                            float dy=event.getY()-startPoint.y;
                            matrix.set(currentMatrix);
                            matrix.postTranslate(dx,dy);
                        }else if (mode==MODE_ZOOM){
                            float ends = setJuli(event);
                            if (ends>10f){
                                float scale=ends/startDis;
                                matrix.set(currentMatrix);
                                matrix.postScale(scale,scale,midPoint.x,midPoint.y);
                            }
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_POINTER_UP:
                        mode=0;
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        mode=MODE_ZOOM;
                        startDis=setJuli(event);
                        if (startDis>10f){
                            midPoint=setPointF(event);
                            currentMatrix.set(imageView.getImageMatrix());
                        }
                        break;
                }
                imageView.setImageMatrix(matrix);
                return true;
            }
            public float setJuli(MotionEvent event){
                float x=event.getX(1)-event.getX(0);
                float y=event.getY(1)-event.getY(0);
                return (float) Math.sqrt(x*x+y*y);

            }

            public PointF setPointF(MotionEvent event){
                float x=(event.getX(1)+event.getX(0))/2;
                float y=(event.getY(1)+event.getY(0))/2;
                return new PointF(x,y);
            }
        });
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_car_error_pictures;
    }
}
