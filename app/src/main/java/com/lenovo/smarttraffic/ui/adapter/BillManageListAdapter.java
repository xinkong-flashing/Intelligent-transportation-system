package com.lenovo.smarttraffic.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.BillManageInfo;
import java.util.List;

public class BillManageListAdapter extends BaseAdapter {

    private List<BillManageInfo> list;
    private LayoutInflater inflater;

    public BillManageListAdapter(Context context, List<BillManageInfo> list) {
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        int res = 0;
        if (list != null) {
            res = list.size();
        }
        return res;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.bill_list_item, null);
            viewHolder._id = convertView.findViewById(R.id.bill_item_id);
            viewHolder.car = convertView.findViewById(R.id.bill_item_car);
            viewHolder.money = convertView.findViewById(R.id.bill_item_money);
            viewHolder.people = convertView.findViewById(R.id.bill_item_people);
            viewHolder.time = convertView.findViewById(R.id.bill_item_time);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (position == 0) {
            viewHolder._id.setText("序号");
            viewHolder.car.setText("车号");
            viewHolder.money.setText("充值金额（元）");
            viewHolder.people.setText("操作人");
            viewHolder.time.setText("充值时间");
        } else {
            viewHolder._id.setText(position+"");
            viewHolder.car.setText(list.get(position).getCar()+"");
            viewHolder.money.setText(list.get(position).getMoney()+"");
            viewHolder.people.setText(list.get(position).getPeople());
            viewHolder.time.setText(list.get(position).getTime());
        }
        return convertView;
    }

    public static class ViewHolder {
        public TextView _id;
        public TextView car;
        public TextView money;
        public TextView people;
        public TextView time;
    }
}
