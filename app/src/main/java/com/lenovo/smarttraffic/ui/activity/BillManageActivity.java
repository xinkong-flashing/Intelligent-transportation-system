package com.lenovo.smarttraffic.ui.activity;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.lenovo.smarttraffic.MainActivity;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.BillManageInfo;
import com.lenovo.smarttraffic.database.dao.AccountDatabase;
import com.lenovo.smarttraffic.ui.adapter.BillManageListAdapter;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BillManageActivity extends BaseActivity {

    private AccountDatabase accountDatabase;
    private ListView listView;
    private SQLiteDatabase db;
    private List<BillManageInfo> list;
    private BillManageInfo info;
    private Spinner spinner;
    private Button button;
    private Context context;
    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义标题栏
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item2));

        listView = findViewById(R.id.bill_list_view);   //ListView控件
        //View控件
        view = findViewById(R.id.bill_view);
        spinner = findViewById(R.id.bill_spinner);  //spinner下拉菜单控件
        button = findViewById(R.id.bill_button);    //查询按钮控件
        context = getApplicationContext();  //获取上下文对象

        //首先需要创建数据库，防止因为没有数据库而使程序崩溃
        accountDatabase = new AccountDatabase(this, "AccountInfo.db", null, 1);
        accountDatabase.getWritableDatabase();

        //判断Account表是否为空
        if (isHaveData()) {     //为空时,删除ListView控件，添加TextView控件
            isNull();
        } else {    //若不为空， 默认按时间降序排序
            list = isHave();
            for (int i = 0; i < list.size() - 1; i++) {
                for (int j = i+1; j < list.size(); j++) {
                    if ((list.get(i).getTime()).compareTo(list.get(j).getTime()) < 0) {
                        list.add(i, list.get(j));
                        list.add(j+1, list.get(i+1));
                        list.remove(i+1);
                        list.remove(j+1);
                    }
                }
            }
            list.add(0, list.get(0));
            BillManageListAdapter adapter = new BillManageListAdapter(context, list);
            listView.setAdapter(adapter);
        }

        //点击查询按钮时
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //如果数据库不为空
                if (!isHaveData()) {
                    list = isHave();
                    //判断是时间升序还是时间降序
                    if ((spinner.getSelectedItem()).equals("时间升序")) {
                        for (int i = 0; i < list.size() - 1; i++) {
                            for (int j = i+1; j < list.size(); j++) {
                                if ((list.get(i).getTime()).compareTo(list.get(j).getTime()) > 0) {
                                    list.add(i, list.get(j));
                                    list.add(j+1, list.get(i+1));
                                    list.remove(i+1);
                                    list.remove(j+1);
                                }
                            }
                        }
                        list.add(0, list.get(0));
                        BillManageListAdapter adapter = new BillManageListAdapter(context, list);
                        listView.setAdapter(adapter);
                    } else {
                        for (int i = 0; i < list.size() - 1; i++) {
                            for (int j = i+1; j < list.size(); j++) {
                                if ((list.get(i).getTime()).compareTo(list.get(j).getTime()) < 0) {
                                    list.add(i, list.get(j));
                                    list.add(j+1, list.get(i+1));
                                    list.remove(i+1);
                                    list.remove(j+1);
                                }
                            }
                        }
                        list.add(0, list.get(0));
                        BillManageListAdapter adapter = new BillManageListAdapter(context, list);
                        listView.setAdapter(adapter);
                    }

                }
            }
        });

    }

    //当表不为空时，获取数据库的值
    public List<BillManageInfo> isHave() {
        db = accountDatabase.getWritableDatabase();
        //查询Account表中的所有数据
        Cursor cursor = db.query("Account", null, null, null, null, null, null);
        list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                info = new BillManageInfo();
                //遍历Cursor对象，取出数据
                info.setCar(cursor.getInt(cursor.getColumnIndex("car")));
                info.setMoney(cursor.getInt(cursor.getColumnIndex("recharge")));
                info.setPeople(cursor.getString(cursor.getColumnIndex("admin")));
                info.setTime(cursor.getString(cursor.getColumnIndex("time")));
                list.add(info);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return list;
    }

    //当表为空时，删除ListView控件，添加TextView控件
    public void isNull() {
        LinearLayout layout = findViewById(R.id.linear_layout);
        view.setVisibility(View.GONE);
        listView.setVisibility(View.GONE);
        TextView textView = new TextView(this);
        textView.setText("暂无历史记录");
        textView.setTextSize(40);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(520, 200, 0, 0);
        layout.addView(textView, params);
    }

    //判断是否有充值记录
    public boolean isHaveData() {
        //查询Account表是否为空
        db = accountDatabase.getWritableDatabase();
        if (db.rawQuery("select * from Account", null).getCount() == 0) {
            return true;
        }
        return false;
    }

    //重写这个方法，设置布局文件
    @Override
    protected int getLayout() {
        return R.layout.activity_bill_manage;
    }

}
