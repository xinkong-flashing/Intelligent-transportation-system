package com.lenovo.smarttraffic.ui.activity;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.lenovo.smarttraffic.MainActivity;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.dialog.MyCarDialog;

import java.util.ArrayList;
import java.util.List;

public class CarAccountActivity extends BaseActivity implements View.OnClickListener {

    private Button oneButton;
    private Button twoButton;
    private Button threeButton;
    private Button foreButton;
    private MyCarDialog myCarDialog;
    private TextView foreCar;
    private TextView threeCar;
    private TextView twoCar;
    private TextView oneCar;
    private CheckBox oneCheckBox;
    private CheckBox twoCheckBox;
    private CheckBox threeCheckBox;
    private CheckBox foreCheckBox;
    private TextView oneBlance;
    private TextView twoBlance;
    private TextView threeBlance;
    private TextView foreBlance;
    private TextView czjl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item9));

        //初始化控件
        initView();

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_car_account;
    }

    public void initView() {
        oneButton = findViewById(R.id.car_account_one_bt);
        twoButton = findViewById(R.id.car_account_two_bt);
        threeButton = findViewById(R.id.car_account_three_bt);
        foreButton = findViewById(R.id.car_account_fore_bt);
        oneButton.setOnClickListener(this);
        twoButton.setOnClickListener(this);
        threeButton.setOnClickListener(this);
        foreButton.setOnClickListener(this);
        oneCar = findViewById(R.id.car_account_one_tv_plate);
        twoCar = findViewById(R.id.car_account_two_tv_plate);
        threeCar = findViewById(R.id.car_account_three_tv_plate);
        foreCar = findViewById(R.id.car_account_fore_tv_plate);
        oneCheckBox = findViewById(R.id.car_account_one_cb);
        twoCheckBox = findViewById(R.id.car_account_two_cb);
        threeCheckBox = findViewById(R.id.car_account_three_cb);
        foreCheckBox = findViewById(R.id.car_account_fore_cb);
        oneBlance = findViewById(R.id.car_account_one_tv_balance);
        twoBlance = findViewById(R.id.car_account_two_tv_balance);
        threeBlance = findViewById(R.id.car_account_three_tv_balance);
        foreBlance = findViewById(R.id.car_account_fore_tv_balance);
        czjl = findViewById(R.id.czjl);
        czjl.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        String[] data = new String[4];
        String[] balance = new String[4];
        switch (view.getId()) {
            case R.id.car_account_one_bt:
                //获取车牌号
                if (!oneCheckBox.isChecked() && !twoCheckBox.isChecked() && !threeCheckBox.isChecked() && !foreCheckBox.isChecked()){
                    data[0] = oneCar.getText().toString();
                    balance[0] = oneBlance.getText().toString();
                } else {
                    data = allPlate();
                    balance = allBalance();
                }
                break;
            case R.id.car_account_two_bt:
                //获取车牌号
                if (!oneCheckBox.isChecked() && !twoCheckBox.isChecked() && !threeCheckBox.isChecked() && !foreCheckBox.isChecked()){
                    data[0] = twoCar.getText().toString();
                    balance[0] = twoBlance.getText().toString();
                } else {
                    data = allPlate();
                    balance = allBalance();
                }
                break;
            case R.id.car_account_three_bt:
                //获取车牌号
                if (!oneCheckBox.isChecked() && !twoCheckBox.isChecked() && !threeCheckBox.isChecked() && !foreCheckBox.isChecked()){
                    data[0] = threeCar.getText().toString();
                    balance[0] = threeBlance.getText().toString();
                } else {
                    data = allPlate();
                    balance = allBalance();
                }
                break;
            case R.id.car_account_fore_bt:
                //获取车牌号
                if (!oneCheckBox.isChecked() && !twoCheckBox.isChecked() && !threeCheckBox.isChecked() && !foreCheckBox.isChecked()){
                    data[0] = foreCar.getText().toString();
                    balance[0] = foreBlance.getText().toString();
                } else {
                    data = allPlate();
                    balance = allBalance();
                }
                break;
            case R.id.czjl:
                Intent intent = new Intent(getApplicationContext(), PersonalCenterActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        //数组去空
        String[] plate = removeDuplicate(data);
        String[] balanceDate = removeDuplicate(balance);
        int[] balances = new int[balanceDate.length];
        for (int i = 0; i < balanceDate.length; i++) {
            balances[i] = Integer.parseInt(balanceDate[i]);
        }
        //实例化自定义的dialog
        myCarDialog = new MyCarDialog(this, R.layout.car_account_curtom_dialog, plate, balances);
        //显示
        myCarDialog.show();
    }

    //获取复选框选中的所有车牌号
    public String[] allPlate() {
        String[] data = new String[4];
        int i = 0;
        if (oneCheckBox.isChecked()) {
            data[i] = oneCar.getText().toString();
            i++;
        }
        if (twoCheckBox.isChecked()) {
            data[i] = twoCar.getText().toString();
            i++;
        }
        if (threeCheckBox.isChecked()) {
            data[i] = threeCar.getText().toString();
            i++;
        }
        if (foreCheckBox.isChecked()) {
            data[i] = foreCar.getText().toString();
            i++;
        }
        return data;
    }
    //获取复选框选中的所有余额
    public String[] allBalance() {
        String[] data = new String[4];
        int i = 0;
        if (oneCheckBox.isChecked()) {
            data[i] = oneBlance.getText().toString();

            i++;
        }
        if (twoCheckBox.isChecked()) {
            data[i] = twoBlance.getText().toString();
            i++;
        }
        if (threeCheckBox.isChecked()) {
            data[i] = threeBlance.getText().toString();
            i++;
        }
        if (foreCheckBox.isChecked()) {
            data[i] = foreBlance.getText().toString();
            i++;
        }
        return data;
    }

    /**
     * 去除重复数组
     */
    public String[] removeDuplicate(String[] data) {
        //声明一个List
        List<String> list = new ArrayList<>();
        for (String str : data) {
            list.add(str);
        }
        //删除null的值
        while (list.remove(null));
        while (list.remove(""));
        //将List转化为数组
        String[] plate = list.toArray(new String[list.size()]);
        return plate;
    }
}
