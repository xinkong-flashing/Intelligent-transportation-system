package com.lenovo.smarttraffic.ui.fragment.PersonalCenter;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lenovo.smarttraffic.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalInformationFragment extends Fragment {


    private View view;

    public PersonalInformationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_personal_information, container, false);

        return view;
    }


}
