package com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.DataAnalysis.AllCarMessageInfo;
import com.lenovo.smarttraffic.bean.DataAnalysis.ErrorCarMessageInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForeFragment extends Fragment {


    private View view;
    private List<AllCarMessageInfo.ROWS_DETAIL> allList;
    private List<ErrorCarMessageInfo.ROWS_DETAIL> errorList;
    private int[] errorDatas;
    private int[] noErrorDatas;
    private WebView webView;

    public ForeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fore, container, false);
        //初始化WebView
        webView = view.findViewById(R.id.data_analysis_webview4);
        webView.loadUrl("file:///android_asset/DataAnalysis/ForeFragment.html");
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        //请求数据
        sendAllCarData();
        return view;
    }

    //请求所有车辆的信息
    public void sendAllCarData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient();
                    RequestBody requestBody = RequestBody.create(MediaType.parse("app/json"),
                            "{\"UserName\":\"user1\"}");
                    Request request = new Request.Builder()
                            .url("http://192.168.1.102:8088/transportservice/action/GetCarInfo.do")
                            .post(requestBody)
                            .build();
                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    //数据解析
                    parseAllCarData(responseData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void parseAllCarData(String jsonData) {
        Gson gson = new Gson();
        AllCarMessageInfo allInfo = gson.fromJson(jsonData, AllCarMessageInfo.class);
        allList = new ArrayList<>();
        allList = allInfo.getROWS_DETAIL();
        //请求违法的车辆信息
        sendErrorCarData();
    }

    //获取违法的车辆信息
    public void sendErrorCarData() {
        try {
            OkHttpClient client = new OkHttpClient();
            RequestBody requestBody = RequestBody.create(MediaType.parse("app/json"),
                    "{\"UserName\":\"user1\"}");
            Request request = new Request.Builder()
                    .url("http://192.168.1.102:8088/transportservice/action/GetAllCarPeccancy.do")
                    .post(requestBody)
                    .build();
            Response response = client.newCall(request).execute();
            String responseData = response.body().string();
            //数据解析
            parseErrorCarData(responseData);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void parseErrorCarData(String jsonData) {
        Gson gson = new Gson();
        ErrorCarMessageInfo errorInfo = gson.fromJson(jsonData, ErrorCarMessageInfo.class);
        errorList = new ArrayList<>();
        errorList = errorInfo.getROWS_DETAIL();
        //集合去重
        for (int i = 0; i < errorList.size() - 1; i++) {
            for (int j = i + 1; j < errorList.size(); j++) {
                if ((errorList.get(i).getCarnumber()).equals(errorList.get(j).getCarnumber())) {
                    errorList.remove(j);
                    j--;
                }
            }
        }
        //获取有违章的车辆的驾驶人年龄
        errorDatas = new int[]{0, 0, 0, 0, 0};
        for (int i = 0; i < errorList.size(); i++) {
            for (int j = 0; j < allList.size(); j++) {
                if ((errorList.get(i).getCarnumber()).equals(allList.get(j).getCarnumber())) {
                    String cardId = allList.get(j).getPcardid();
                    if (cardId.charAt(8) == '9') {
                        errorDatas[0]++;
                    } else if (cardId.charAt(8) == '8'){
                        errorDatas[1]++;
                    } else if (cardId.charAt(8) == '7'){
                        errorDatas[2]++;
                    } else if (cardId.charAt(8) == '6'){
                        errorDatas[3]++;
                    } else if (cardId.charAt(8) == '5'){
                        errorDatas[4]++;
                    }
                }
            }
        }

        //遍历所有车辆信息，删除违章的车辆，得到为违章的车辆
        for (int i = 0; i < errorList.size(); i++) {
            for (int j = 0; j < allList.size(); j++){
                if ((allList.get(j).getCarnumber()).equals(errorList.get(i).getCarnumber())) {
                    allList.remove(j);
                    j--;
                }
            }
        }
        //获取无违章信息的车辆驾驶人的年龄
        noErrorDatas = new int[]{0, 0, 0, 0, 0};
        for (int i = 0; i < allList.size(); i++) {
            String cardId = allList.get(i).getPcardid();
            if (cardId.charAt(8) == '9') {
                noErrorDatas[0]++;
            } else if (cardId.charAt(8) == '8') {
                noErrorDatas[1]++;
            } else if (cardId.charAt(8) == '7') {
                noErrorDatas[2]++;
            } else if (cardId.charAt(8) == '6') {
                noErrorDatas[3]++;
            } else if (cardId.charAt(8) == '5') {
                noErrorDatas[4]++;
            }
        }

        //向WebView传递数据
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:getData(\""+ Arrays.toString(errorDatas) +"\", \""+Arrays.toString(noErrorDatas)+"\")");
            }
        });

    }
}
