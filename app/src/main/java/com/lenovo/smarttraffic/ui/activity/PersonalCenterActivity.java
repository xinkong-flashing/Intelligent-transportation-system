package com.lenovo.smarttraffic.ui.activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.database.dao.CarAccountDataBase;
import com.lenovo.smarttraffic.ui.fragment.PersonalCenter.PersonalInformationFragment;
import com.lenovo.smarttraffic.ui.fragment.PersonalCenter.RechargeRecordFragment;
import com.lenovo.smarttraffic.ui.fragment.PersonalCenter.ThresholdSettingFragment;

public class PersonalCenterActivity extends BaseActivity implements View.OnClickListener {

    private TextView tv1;
    private TextView tv2;
    private TextView tv3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义导航栏
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item0));
        //初始化控件
        init();
        //初始时显示个人信息碎片
        replaceFragment(new PersonalInformationFragment());
    }

    //定义替换Fragment的方法
    public void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.personal_center_fragment, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.personal_center_tv1:
                replaceFragment(new PersonalInformationFragment());
                tv1.setBackgroundResource(R.drawable.personal_center_toolbar_tv);
                tv2.setBackgroundResource(R.color.ccc);
                tv3.setBackgroundResource(R.color.ccc);
                break;
            case R.id.personal_center_tv2:
                replaceFragment(new RechargeRecordFragment());
                tv1.setBackgroundResource(R.color.ccc);
                tv2.setBackgroundResource(R.drawable.personal_center_toolbar_tv);
                tv3.setBackgroundResource(R.color.ccc);
                break;
            case R.id.personal_center_tv3:
                replaceFragment(new ThresholdSettingFragment());
                tv1.setBackgroundResource(R.color.ccc);
                tv2.setBackgroundResource(R.color.ccc);
                tv3.setBackgroundResource(R.drawable.personal_center_toolbar_tv);
                break;
        }
    }

    //初始化控件
    public void init() {
        tv1 = findViewById(R.id.personal_center_tv1);
        tv1.setOnClickListener(this::onClick);
        tv2 = findViewById(R.id.personal_center_tv2);
        tv2.setOnClickListener(this::onClick);
        tv3 = findViewById(R.id.personal_center_tv3);
        tv3.setOnClickListener(this::onClick);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_personal_center;
    }

}
