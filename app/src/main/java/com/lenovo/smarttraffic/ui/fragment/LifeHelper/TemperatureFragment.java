package com.lenovo.smarttraffic.ui.fragment.LifeHelper;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.database.dao.LifeHelperDatabase;

import org.w3c.dom.Text;

import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class TemperatureFragment extends Fragment {


    private View view;
    private LifeHelperDatabase lifeHelperDatabase;
    private WebView webView;

    public TemperatureFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_temperature, container, false);
        //创建数据库
        lifeHelperDatabase = new LifeHelperDatabase(getContext(), "LifeHelper.db", null, 1);
        lifeHelperDatabase.getWritableDatabase();
        //初始化webView
        webView = view.findViewById(R.id.life_helper_temperature_fragment_webview);
        webView.loadUrl("file:///android_asset/LifeHelper/LineGraph.html");
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        //handler每3秒发送一次消息
        handler.sendEmptyMessage(0x012);
        return view;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0x012){
                int n = 0;
                int[] data = new int[20];
                String[] time = new String[20];
                SQLiteDatabase db = lifeHelperDatabase.getWritableDatabase();
                Cursor cursor = db.query("AllWeather", null, null, null, null, null, null);
                if (cursor.moveToFirst()) {
                    do {
                        data[n] = cursor.getInt(cursor.getColumnIndex("temperature"));
                        time[n] = cursor.getString(cursor.getColumnIndex("time"));
                        n++;
                    }while (cursor.moveToNext());
                }
                cursor.close();
                //找出这组数据的最高温度和最低温度
                int maxData = 0;
                int minData = 1000;
                for (int i = 0; i < data.length; i++) {
                    if (data[i] > maxData) {
                        maxData = data[i];
                    }
                    if (data[i] < minData) {
                        minData = data[i];
                    }
                }
                TextView textView = view.findViewById(R.id.life_helper_temperature_fragment_textview);
                textView.setText("过去1分钟的最高气温："+maxData+"℃， 最低气温："+minData+"℃");
                //传递数据给web
                webView.loadUrl("javascript:getData(\""+ Arrays.toString(time) +"\", \""+Arrays.toString(data)+"\")");
            }
            handler.sendEmptyMessageDelayed(0x012, 3000);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
}
