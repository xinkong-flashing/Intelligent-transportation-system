package com.lenovo.smarttraffic.ui.activity;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.TrafficLightInfo;
import com.lenovo.smarttraffic.ui.adapter.TrafficLightListAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TrafficLightManageActivity extends BaseActivity {

    private List<TrafficLightInfo> list;
    private ListView listView;
    private Spinner spinner;
    private Button button;
    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义标题栏
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item3));

        listView = findViewById(R.id.traffic_light_list_view);
        spinner = findViewById(R.id.traffic_light_spinner);
        button = findViewById(R.id.traffic_light_button);

        //添加数据
        list = setList();

        //默认以路口升序显示
        TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), crossUp(), onClickListener);
        listView.setAdapter(adapter);



        //点击查询时，判断spinner的选中值
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((spinner.getSelectedItem()).equals("路口升序")){
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), crossUp(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("路口降序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), crossDown(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("红灯升序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), redUp(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("红灯降序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), redDown(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("黄灯升序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), yellowUp(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("黄灯降序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), yellowDown(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("绿灯升序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), greenUp(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("绿灯降序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), greenDown(), onClickListener);
                    listView.setAdapter(adapter);
                }
            }
        });
    }

    //ListView中Button点击事件
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Button button = (Button) view;
            //获取ListView的position值
            pos = (Integer) button.getTag();
            //自定义Dialog
            curtomDialog();
        }
    };

    //自定义Dialog
    private void curtomDialog() {
        AlertDialog dialog = new AlertDialog.Builder(TrafficLightManageActivity.this).create();
        View view = LayoutInflater.from(this.getBaseContext()).inflate(R.layout.traffic_light_curtom_dialog, null, false);
        dialog.setView(view);
        dialog.setCanceledOnTouchOutside(false);   //点击外部dialog不消失
        dialog.show();
        //dialog中的点击事件
        Button btnConfirm = view.findViewById(R.id.traffic_light_dialog_confirm);
        Button btnCancel = view.findViewById(R.id.traffic_light_dialog_cancel);
        EditText etRed = view.findViewById(R.id.traffic_light_dialog_red);
        EditText etYellow = view.findViewById(R.id.traffic_light_dialog_yellow);
        EditText etGreen = view.findViewById(R.id.traffic_light_dialog_green);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String setRed = etRed.getText().toString();
                String setYellow = etYellow.getText().toString();
                String setGreen = etGreen.getText().toString();
                list.set(pos, new TrafficLightInfo(list.get(pos).getCross(),
                        Integer.parseInt(setRed), Integer.parseInt(setYellow), Integer.parseInt(setGreen)));
                if ((spinner.getSelectedItem()).equals("路口升序")){
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), crossUp(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("路口降序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), crossDown(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("红灯升序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), redUp(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("红灯降序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), redDown(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("黄灯升序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), yellowUp(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("黄灯降序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), yellowDown(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("绿灯升序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), greenUp(), onClickListener);
                    listView.setAdapter(adapter);
                } else if ((spinner.getSelectedItem()).equals("绿灯降序")) {
                    TrafficLightListAdapter adapter = new TrafficLightListAdapter(getApplicationContext(), greenDown(), onClickListener);
                    listView.setAdapter(adapter);
                }
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    //添加数据
    public List<TrafficLightInfo> setList() {
        list = new ArrayList<>();
        list.add(new TrafficLightInfo(1, 25, 28, 17));
        list.add(new TrafficLightInfo(1, 25, 28, 17));
        list.add(new TrafficLightInfo(2, 32, 19, 28));
        list.add(new TrafficLightInfo(3, 28, 30, 24));
        list.add(new TrafficLightInfo(4, 20, 27, 35));
        list.add(new TrafficLightInfo(5, 23, 21, 20));
        return list;
    }

    //路口升序排序
    public List<TrafficLightInfo> crossUp() {
        list.remove(0);
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if ((list.get(i).getCross()) > list.get(j).getCross()) {
                    list.add(i, list.get(j));
                    list.add(j+1, list.get(i+1));
                    list.remove(i+1);
                    list.remove(j+1);
                }
            }
        }
        list.add(0, list.get(0));
        return list;
    }
    //路口降序排序
    public List<TrafficLightInfo> crossDown() {
        list.remove(0);
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if ((list.get(i).getCross()) < list.get(j).getCross()) {
                    list.add(i, list.get(j));
                    list.add(j+1, list.get(i+1));
                    list.remove(i+1);
                    list.remove(j+1);
                }
            }
        }
        list.add(0, list.get(0));
        return list;
    }
    //红灯升序排序
    public List<TrafficLightInfo> redUp() {
        list.remove(0);
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if ((list.get(i).getRed()) > list.get(j).getRed()) {
                    list.add(i, list.get(j));
                    list.add(j+1, list.get(i+1));
                    list.remove(i+1);
                    list.remove(j+1);
                }
            }
        }
        list.add(0, list.get(0));
        return list;
    }
    //红灯降序排序
    public List<TrafficLightInfo> redDown() {
        list.remove(0);
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if ((list.get(i).getRed()) < list.get(j).getRed()) {
                    list.add(i, list.get(j));
                    list.add(j+1, list.get(i+1));
                    list.remove(i+1);
                    list.remove(j+1);
                }
            }
        }
        list.add(0, list.get(0));
        return list;
    }
    //黄灯升序排序
    public List<TrafficLightInfo> yellowUp() {
        list.remove(0);
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if ((list.get(i).getYellow()) > list.get(j).getYellow()) {
                    list.add(i, list.get(j));
                    list.add(j+1, list.get(i+1));
                    list.remove(i+1);
                    list.remove(j+1);
                }
            }
        }
        list.add(0, list.get(0));
        return list;
    }
    //黄灯降序排序
    public List<TrafficLightInfo> yellowDown() {
        list.remove(0);
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if ((list.get(i).getYellow()) < list.get(j).getYellow()) {
                    list.add(i, list.get(j));
                    list.add(j+1, list.get(i+1));
                    list.remove(i+1);
                    list.remove(j+1);
                }
            }
        }
        list.add(0, list.get(0));
        return list;
    }
    //绿灯升序排序
    public List<TrafficLightInfo> greenUp() {
        list.remove(0);
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if ((list.get(i).getGreen()) > list.get(j).getGreen()) {
                    list.add(i, list.get(j));
                    list.add(j+1, list.get(i+1));
                    list.remove(i+1);
                    list.remove(j+1);
                }
            }
        }
        list.add(0, list.get(0));
        return list;
    }
    //绿灯降序排序
    public List<TrafficLightInfo> greenDown() {
        list.remove(0);
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i+1; j < list.size(); j++) {
                if ((list.get(i).getGreen()) < list.get(j).getGreen()) {
                    list.add(i, list.get(j));
                    list.add(j+1, list.get(i+1));
                    list.remove(i+1);
                    list.remove(j+1);
                }
            }
        }
        list.add(0, list.get(0));
        return list;
    }

    //重写这个方法，设置布局文件
    @Override
    protected int getLayout() {
        return R.layout.activity_traffic_light_manage;
    }

    //数据请求
    private void sendRequestWithOk() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient okHttpClient = new OkHttpClient();
                    RequestBody requestBody = new FormBody.Builder()
                            .add("TrafficLightId", "1")
                            .add("UserName", "user1")
                            .build();
                    Request request = new Request.Builder()
                            .url("http://localhost:8088/transportservice/action/GetTrafficLightConfigAction.do")
                            .post(requestBody)
                            .build();
                    Response response = okHttpClient.newCall(request).execute();
                    String responseData = response.body().string();
                    //数据分析
                    parseJsonWithGson(responseData);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    //使用Gson解析Json数据
    private void parseJsonWithGson(String jsonData) {
        Gson gson = new Gson();


    }
}
