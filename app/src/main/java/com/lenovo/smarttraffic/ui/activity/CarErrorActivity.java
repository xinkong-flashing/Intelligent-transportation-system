package com.lenovo.smarttraffic.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.CarErrorBean;
import com.lenovo.smarttraffic.bean.ROWS_DETAIL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CarErrorActivity extends BaseActivity {

    private List<ROWS_DETAIL> list;
    private EditText editText;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义标题栏
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item11));

        editText = findViewById(R.id.car_error_editview);
        button = findViewById(R.id.car_error_button);

        //数据请求
        sendRequestWithOkHttp();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < list.size(); i++) {
                    if ((list.get(i).getCarnumber()).equals("鲁"+editText.getText())) {
                        Intent intent = new Intent(getApplicationContext(), CarErrorResultActivity.class);
                        intent.putExtra("carnumber", "鲁"+editText.getText());
                        startActivity(intent);
                        return;
                    }
                }
                Toast.makeText(CarErrorActivity.this, "没有查询到 鲁"+editText.getText()+" 车的违章数据！", Toast.LENGTH_SHORT).show();
            }
        });

    }

    //OkHttp网络请求数据
    private void sendRequestWithOkHttp() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient();
                    RequestBody requestBody = RequestBody.create(MediaType.parse("app/json"),"{\"UserName\":\"user1\"}");
                    Request request = new Request.Builder()
                            .url("http://192.168.1.102:8088/transportservice/action/GetAllCarPeccancy.do")
                            .post(requestBody)
                            .build();
                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    //数据解析
                    parseJsonWithGson(responseData);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    //使用Gsom数据解析
    private void parseJsonWithGson(String jsonData) {
        Gson gson = new Gson();
        CarErrorBean bean = gson.fromJson(jsonData, CarErrorBean.class);
        list = new ArrayList<>();
        list = bean.getROWS_DETAIL();
    }

    //如果活动不可见就直接杀掉
    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_car_error;
    }
}
