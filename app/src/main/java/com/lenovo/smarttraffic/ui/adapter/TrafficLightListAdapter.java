package com.lenovo.smarttraffic.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.TrafficLightInfo;

import org.w3c.dom.Text;

import java.util.List;

public class TrafficLightListAdapter extends BaseAdapter {

    private Context context;
    private List<TrafficLightInfo> list;
    private View.OnClickListener onClickListener;

    public TrafficLightListAdapter(Context context, List<TrafficLightInfo> list, View.OnClickListener onClickListener) {
        this.context = context;
        this.list = list;
        this.onClickListener = onClickListener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.traffic_light_list_item, null);
            viewHolder.cross = convertView.findViewById(R.id.traffic_light_item_cross);
            viewHolder.red = convertView.findViewById(R.id.traffic_light_item_red);
            viewHolder.yellow = convertView.findViewById(R.id.traffic_light_item_yellow);
            viewHolder.green = convertView.findViewById(R.id.traffic_light_item_green);
            viewHolder.lcb = convertView.findViewById(R.id.traffic_light_item_lcb);
            viewHolder.lbt = convertView.findViewById(R.id.traffic_light_item_lbt);
            viewHolder.checkBox = convertView.findViewById(R.id.traffic_light_item_checkbox);
            viewHolder.button = convertView.findViewById(R.id.traffic_light_item_button);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (position == 0) {
            viewHolder.cross.setText("路口");
            viewHolder.red.setText("红灯时长（s）");
            viewHolder.yellow.setText("黄灯时长（s）");
            viewHolder.green.setText("绿灯时长（s）");
            viewHolder.lcb.removeView(viewHolder.checkBox);
            viewHolder.lbt.removeView(viewHolder.button);
            TextView tvOperation = new TextView(context);
            tvOperation.setText("操作项");
            tvOperation.setTextSize(20);
            tvOperation.setWidth(115);
            tvOperation.setGravity(Gravity.CENTER);
            tvOperation.setTextColor(Color.BLACK);
            viewHolder.lcb.addView(tvOperation);
            TextView tvSetting = new TextView(context);
            tvSetting.setText("设置");
            tvSetting.setTextSize(20);
            tvSetting.setWidth(230);
            tvSetting.setGravity(Gravity.CENTER);
            tvSetting.setTextColor(Color.BLACK);
            viewHolder.lbt.addView(tvSetting);
        } else {
            viewHolder.cross.setText(list.get(position).getCross()+"");
            viewHolder.red.setText(list.get(position).getRed()+"");
            viewHolder.yellow.setText(list.get(position).getYellow()+"");
            viewHolder.green.setText(list.get(position).getGreen()+"");
            //通常将position设置为tag，方便之后判断点击的button是哪一个
            viewHolder.button.setTag(position);
            viewHolder.button.setOnClickListener(this.onClickListener);
        }
        return convertView;
    }
    public class ViewHolder{
        TextView cross;
        TextView red;
        TextView yellow;
        TextView green;
        LinearLayout lcb;
        LinearLayout lbt;
        CheckBox checkBox;
        Button button;
    }
}
