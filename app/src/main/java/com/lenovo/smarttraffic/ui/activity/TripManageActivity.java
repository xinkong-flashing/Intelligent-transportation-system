package com.lenovo.smarttraffic.ui.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Switch;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TripManageActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvTime;
    private TextView tvCar;
    private Calendar calendar = Calendar.getInstance(Locale.CANADA);
    private Switch threeSwitch;
    private Switch twoSwitch;
    private Switch oneSwitch;
    private TextView tvOne;
    private TextView tvTwo;
    private TextView tvThree;
    private View redView;
    private View yellowView;
    private View greenView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义导航栏
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item8));

        tvTime = findViewById(R.id.trip_tv_time);
        tvTime.setOnClickListener(this);
        tvCar = findViewById(R.id.trip_tv_car);
        oneSwitch = findViewById(R.id.trip_one_switch);
        twoSwitch = findViewById(R.id.trip_two_switch);
        threeSwitch = findViewById(R.id.trip_three_switch);
        tvOne = findViewById(R.id.trip_one);
        tvTwo = findViewById(R.id.trip_two);
        tvThree = findViewById(R.id.trip_three);
        redView = findViewById(R.id.trip_view_red);
        yellowView = findViewById(R.id.trip_view_yellow);
        greenView = findViewById(R.id.trip_view_green);

        //初始化时间和出行车辆
        startTime();

        //Switch控件监听器
        oneSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    tvOne.setText("开");
                } else {
                    tvOne.setText("停");
                }
            }
        });
        twoSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    tvTwo.setText("开");
                } else {
                    tvTwo.setText("停");
                }
            }
        });
        threeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    tvThree.setText("开");
                } else {
                    tvThree.setText("停");
                }
            }
        });

        //动画加载
        handler.sendEmptyMessage(0x111);

    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0x111) {
                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        greenHandler.sendEmptyMessage(0x003);
                        SystemClock.sleep(2000);
                        redHandler.sendEmptyMessage(0x001);
                        SystemClock.sleep(2000);
                        yellowHandler.sendEmptyMessage(0x002);
                        SystemClock.sleep(2000);

                    }
                }.start();
                handler.sendEmptyMessageDelayed(0x111, 6000);
            }
        }
    };

    Handler redHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0x001){
                redView.setVisibility(View.VISIBLE);
                yellowView.setVisibility(View.INVISIBLE);
            }
        }
    };
    Handler yellowHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0x002){
                yellowView.setVisibility(View.VISIBLE);
                greenView.setVisibility(View.INVISIBLE);
            }
        }
    };
    Handler greenHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0x003){
                redView.setVisibility(View.INVISIBLE);
                greenView.setVisibility(View.VISIBLE);
            }
        }
    };

    public void startTime() {
        //获取当前时间并设置
        SimpleDateFormat df = new SimpleDateFormat("YYYY年MM月dd日"); //设置日期格式
        String date = df.format(new Date());
        tvTime.setText(date);
        //获取单一日分，判断是出行车辆
        SimpleDateFormat dd = new SimpleDateFormat("dd");
        String day = dd.format(new Date());
        if (Integer.parseInt(day) % 2 == 1) {
            tvCar.setText("单号出行车辆：1、3");
            odd();
        } else {
            tvCar.setText("双号出行车辆：2");
            event();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.trip_tv_time:
                showDatePickerDialog(4, calendar);
                break;
        }
    }

    public void showDatePickerDialog(int themeResId, Calendar calendar) {
        // 直接创建一个DatePickerDialog对话框实例，并将它显示出来
        new DatePickerDialog(this, themeResId, new DatePickerDialog.OnDateSetListener() {
            // 绑定监听器(How the parent is notified that the date is set.)
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // 此处得到选择的时间，可以进行你想要的操作
                tvTime.setText(year + "年" + (monthOfYear + 1) + "月" + dayOfMonth + "日");
                if (dayOfMonth % 2 == 1) {
                    tvCar.setText("单号出行车辆：1、3");
                    odd();
                } else {
                    tvCar.setText("双号出行车辆：2");
                    event();
                }
            }
        }
                // 设置初始日期
                , calendar.get(Calendar.YEAR)
                , calendar.get(Calendar.MONTH)
                , calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void odd() {
        twoSwitch.setEnabled(false);    //设置控件不可用
        oneSwitch.setEnabled(true);
        threeSwitch.setEnabled(true);
    }

    public void event() {
        oneSwitch.setEnabled(false);
        threeSwitch.setEnabled(false);
        twoSwitch.setEnabled(true);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_trip_manage;
    }

    //注销handler消息传递
    @Override
    protected void onDestroy() {
        super.onDestroy();
        redHandler.removeCallbacksAndMessages(null);
        yellowHandler.removeCallbacksAndMessages(null);
        greenHandler.removeCallbacksAndMessages(null);
        handler.removeCallbacksAndMessages(null);
    }
}
