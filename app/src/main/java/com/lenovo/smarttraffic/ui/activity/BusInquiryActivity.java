package com.lenovo.smarttraffic.ui.activity;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.dialog.BusInquiryDialog;

public class BusInquiryActivity extends BaseActivity {

    private final int TIMER_MSG = 0x001;    //定义消息代码
    private TextView tvDistance1;
    private TextView tvDistance2;
    private TextView tvDistance3;
    private TextView tvDistance4;
    private TextView tvTime1;
    private TextView tvTime2;
    private TextView tvTime3;
    private TextView tvTime4;
    private TextView tvPeople1;
    private TextView tvPeople2;
    private TextView tvPeople3;
    private TextView tvPeople4;
    private boolean updown1;
    private boolean updown2;
    private boolean updown3;
    private boolean updown4;
    private Button button;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义导航栏
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item10));

        //注册控件
        info();

        //使用handler3秒更新一次数据
        handler.sendEmptyMessage(TIMER_MSG);

        //点击按钮打开详细乘客总数
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //实例化自定义的dialog
                BusInquiryDialog busInquiryDialog = new BusInquiryDialog(context, R.layout.bus_inquiry_curtom_dialog);
                //显示dialog
                busInquiryDialog.show();
            }
        });

    }

    //使用handler3秒更新一次数据
    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == TIMER_MSG) {
                //1号公交车
                int distance1;
                int busDistance1 = Integer.parseInt(tvDistance1.getText().toString());
                if (busDistance1 == 100) {
                    updown1 = true;
                }
                if (busDistance1 == 0) {
                    updown1 = false;
                }
                if (updown1) { //降序
                    distance1 = computationDistanceDown(busDistance1);  //计算剩余的距离
                } else {    //升序
                    distance1 = computationDistanceUp(busDistance1);  //计算剩余的距离
                }
                if (distance1 < 0) {
                    distance1 = 0;
                }
                if (distance1 > 100) {
                    distance1 = 100;
                }
                int time1 = computationTime(distance1); //计算剩余的时间
                int people1 = computationPeople();  //计算乘客人数
                //赋值
                tvPeople1.setText(people1+"");
                tvTime1.setText(time1+"");
                tvDistance1.setText(distance1+"");
                //2号公交车
                int distance2;
                int busDistance2 = Integer.parseInt(tvDistance2.getText().toString());
                if (busDistance2 == 1000) {
                    updown2 = true;
                }
                if (busDistance2 == 0) {
                    updown2 = false;
                }
                if (updown2) { //降序
                    distance2 = computationDistanceDown(busDistance2);  //计算剩余的距离
                } else {    //升序
                    distance2 = computationDistanceUp(busDistance2);  //计算剩余的距离
                }
                if (distance2 < 0) {
                    distance2 = 0;
                }
                if (distance2 > 1000) {
                    distance2 = 1000;
                }
                int time2 = computationTime(distance2); //计算剩余时间
                int people2 = computationPeople();  //计算乘客人数
                //赋值
                Log.d("text", people1+"");
                tvPeople2.setText(people2+"");
                tvTime2.setText(time2+"");
                tvDistance2.setText(distance2+"");
                //3号公交车
                int distance3;
                int busDistance3 = Integer.parseInt(tvDistance3.getText().toString());
                if (busDistance3 == 500) {
                    updown3 = true;
                }
                if (busDistance3 == 0) {
                    updown3 = false;
                }
                if (updown3) { //降序
                    distance3 = computationDistanceDown(busDistance3);  //计算剩余的距离
                } else {    //升序
                    distance3 = computationDistanceUp(busDistance3);  //计算剩余的距离
                }
                if (distance3 < 0) {
                    distance3 = 0;
                }
                if (distance3 > 500) {
                    distance3 = 500;
                }
                int time3 = computationTime(distance3); //计算剩余的时间
                int people3 = computationPeople();  //计算乘客人数
                //赋值
                tvPeople3.setText(people3+"");
                tvTime3.setText(time3+"");
                tvDistance3.setText(distance3+"");
                //4号公交车
                int distance4;
                int busDistance4 = Integer.parseInt(tvDistance4.getText().toString());
                if (busDistance4 == 1400) {
                    updown4 = true;
                }
                if (busDistance4 == 0) {
                    updown4 = false;
                }
                if (updown4) { //降序
                    distance4 = computationDistanceDown(busDistance4);  //计算剩余的距离
                } else {    //升序
                    distance4 = computationDistanceUp(busDistance4);  //计算剩余的距离
                }
                if (distance4 < 0) {
                    distance4 = 0;
                }
                if (distance4 > 1400) {
                    distance4 = 1400;
                }
                int time4 = computationTime(distance4); //计算剩余的时间
                int people4 = computationPeople();  //计算乘客人数
                //赋值
                tvPeople4.setText(people4+"");
                tvTime4.setText(time4+"");
                tvDistance4.setText(distance4+"");
                //handler发送消息
                handler.sendEmptyMessageDelayed(TIMER_MSG, 3000);
            }
        }
    };
    //计算公交车的载客人数（随机数）
    public int computationPeople() {
        double datas = Math.random()*100;
        int data = (int) datas;
        return data;
    }

    //计算公交车距离站台的距离(降序)
    public int computationDistanceDown(int distance) {
        return distance - (20000/60/60);
    }

    //计算公交车距离站台的距离(升序)
    public int computationDistanceUp(int distance) {
        return distance + (20000/60/60);
    }

    //计算公交车的到达时间
    public int computationTime(int distance) {
        int second = distance/(20000/60/60);
        int minute = (second/60)+1;
        return minute;
    }

    //控件注册
    public  void info() {
        tvDistance1 = findViewById(R.id.bus1_tv_distance);
        tvDistance2 = findViewById(R.id.bus2_tv_distance);
        tvDistance3 = findViewById(R.id.bus3_tv_distance);
        tvDistance4 = findViewById(R.id.bus4_tv_distance);
        tvTime1 = findViewById(R.id.bus1_tv_time);
        tvTime2 = findViewById(R.id.bus2_tv_time);
        tvTime3 = findViewById(R.id.bus3_tv_time);
        tvTime4 = findViewById(R.id.bus4_tv_time);
        tvPeople1 = findViewById(R.id.bus1_tv_people);
        tvPeople2 = findViewById(R.id.bus2_tv_people);
        tvPeople3 = findViewById(R.id.bus3_tv_people);
        tvPeople4 = findViewById(R.id.bus4_tv_people);
        button = findViewById(R.id.bus_button);
        context = this;

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_bus_inquiry;
    }

    //关闭活动时，停止handler

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
}
