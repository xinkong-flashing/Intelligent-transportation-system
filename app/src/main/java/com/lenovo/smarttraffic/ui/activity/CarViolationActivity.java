package com.lenovo.smarttraffic.ui.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.lenovo.smarttraffic.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CarViolationActivity extends BaseActivity {

    private GridView gvVideo;
    private List<Map<String, Object>> data_list;
    private GridView gvPicture;
    private String[] videoName = {"宝马汽车装配", "车辆管理", "红绿灯管理",
            "跑车宣传", "汽车历史教育", "汽车历史视频", "我的座驾", "智能交通"};
    private int[] pictures = {R.drawable.car_video_p1, R.drawable.car_video_p2, R.drawable.car_video_p3,
            R.drawable.car_video_p4, R.drawable.car_video_p5, R.drawable.car_video_p6,
            R.drawable.car_video_p7, R.drawable.car_video_p8};
    private VideoView videoView;
    private Window windowVideo;
    private WindowManager.LayoutParams lpVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义标题栏
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item5));

        //选项卡设置
        TabHost tabHost = findViewById(android.R.id.tabhost);
        tabHost.setup();    //初始化选项卡
        //为选项卡添加标签页
        LayoutInflater inflater = LayoutInflater.from(this);
        inflater.inflate(R.layout.car_violation_tabvideo, tabHost.getTabContentView());
        inflater.inflate(R.layout.car_violation_tabpicture, tabHost.getTabContentView());
        tabHost.addTab(tabHost.newTabSpec("video").setIndicator("违章视频").setContent(R.id.car_violation_tabvideo));
        tabHost.addTab(tabHost.newTabSpec("picture").setIndicator("违章图片").setContent(R.id.car_violation_tabpicture));
        //自定义选项卡的字体大小
        TabWidget tabWidget = tabHost.getTabWidget();
        for (int i = 0; i < tabWidget.getChildCount(); i++) {
            //修改TabHost的高度
            tabWidget.getChildAt(i).getLayoutParams().height = 100;
            //修改显示字体的大小
            TextView tv = tabWidget.getChildAt(i).findViewById(android.R.id.title);
            tv.setTextSize(40);
            tv.setTextColor(Color.BLACK);
        }

        //GridView Video设置
        gvVideo = findViewById(R.id.car_violation_gvvideo);
        //新建List
        data_list = new ArrayList<>();
        //获取数据
        getData();
        //新建适配器
        String[] from = {"image", "text"};
        int[] to = {R.id.car_violation_video_image, R.id.car_violation_video_text};
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, data_list, R.layout.car_violation_video_item, from, to);
        //配置适配器
        gvVideo.setAdapter(simpleAdapter);
        //配置视频播放对话框
        videoView = new VideoView(this);
        Dialog dialogVideo = new Dialog(this);
        dialogVideo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        windowVideo = dialogVideo.getWindow();
        lpVideo = windowVideo.getAttributes();
        lpVideo.x = 0;
        lpVideo.y = 0;
        lpVideo.height = WindowManager.LayoutParams.MATCH_PARENT;
        lpVideo.width = WindowManager.LayoutParams.MATCH_PARENT;
        //注册监听事件
        gvVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        File file0 = new File(Environment.getExternalStorageDirectory() + "/宝马汽车装配.3gp");//获取文件对象
                        videoView.setVideoPath(file0.getAbsolutePath()); //指定要播放的视频
                        dialogVideo.setContentView(videoView);
                        windowVideo.setAttributes(lpVideo);
                        dialogVideo.show();
                        videoView.start();
                        break;
                    case 1:
                        File file1 = new File(Environment.getExternalStorageDirectory() + "/车辆管理.mp4");//获取文件对象
                        videoView.setVideoPath(file1.getAbsolutePath()); //指定要播放的视频
                        dialogVideo.setContentView(videoView);
                        windowVideo.setAttributes(lpVideo);
                        dialogVideo.show();
                        videoView.start();
                        break;
                    case 2:
                        File file2 = new File(Environment.getExternalStorageDirectory() + "/红绿灯管理.mp4");//获取文件对象
                        videoView.setVideoPath(file2.getAbsolutePath()); //指定要播放的视频
                        dialogVideo.setContentView(videoView);
                        windowVideo.setAttributes(lpVideo);
                        dialogVideo.show();
                        videoView.start();
                        break;
                    case 3:
                        File file3 = new File(Environment.getExternalStorageDirectory() + "/跑车宣传.3gp");//获取文件对象
                        videoView.setVideoPath(file3.getAbsolutePath()); //指定要播放的视频
                        dialogVideo.setContentView(videoView);
                        windowVideo.setAttributes(lpVideo);
                        dialogVideo.show();
                        videoView.start();
                        break;
                    case 4:
                        File file4 = new File(Environment.getExternalStorageDirectory() + "/汽车历史教育.3gp");//获取文件对象
                        videoView.setVideoPath(file4.getAbsolutePath()); //指定要播放的视频
                        dialogVideo.setContentView(videoView);
                        windowVideo.setAttributes(lpVideo);
                        dialogVideo.show();
                        videoView.start();
                        break;
                    case 5:
                        File file5 = new File(Environment.getExternalStorageDirectory() + "/汽车历史视频.3gp");//获取文件对象
                        videoView.setVideoPath(file5.getAbsolutePath()); //指定要播放的视频
                        dialogVideo.setContentView(videoView);
                        windowVideo.setAttributes(lpVideo);
                        dialogVideo.show();
                        videoView.start();
                        break;
                    case 6:
                        File file6 = new File(Environment.getExternalStorageDirectory() + "/我的座驾.mp4");//获取文件对象
                        videoView.setVideoPath(file6.getAbsolutePath()); //指定要播放的视频
                        dialogVideo.setContentView(videoView);
                        windowVideo.setAttributes(lpVideo);
                        dialogVideo.show();
                        videoView.start();
                        break;
                    case 7:
                        File file7 = new File(Environment.getExternalStorageDirectory() + "/智能交通.mp4");//获取文件对象
                        videoView.setVideoPath(file7.getAbsolutePath()); //指定要播放的视频
                        dialogVideo.setContentView(videoView);
                        windowVideo.setAttributes(lpVideo);
                        dialogVideo.show();
                        videoView.start();
                        break;
                }
            }
        });

        //GridView Picture设置
        gvPicture = findViewById(R.id.car_violation_gvpicture);
        gvPicture.setAdapter(new ImageAdapter(this));
        //弹出对话框显示图片
        ImageView imageView = new ImageView(this);
        Dialog dialogPicture = new Dialog(this);
        dialogPicture.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window windowPicture = dialogPicture.getWindow();
        WindowManager.LayoutParams lpPicture = windowPicture.getAttributes();
        lpPicture.height = 150;
        lpPicture.width = 150;

        //注册picture的监听事件
        gvPicture.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        imageView.setImageResource(R.drawable.car_video_p1);
                        dialogPicture.setContentView(imageView);
                        windowPicture.setAttributes(lpPicture);
                        dialogPicture.show();
                        break;
                    case 1:
                        imageView.setImageResource(R.drawable.car_video_p2);
                        dialogPicture.setContentView(imageView);
                        windowPicture.setAttributes(lpPicture);
                        dialogPicture.show();
                        break;
                    case 2:
                        imageView.setImageResource(R.drawable.car_video_p3);
                        dialogPicture.setContentView(imageView);
                        windowPicture.setAttributes(lpPicture);
                        dialogPicture.show();
                        break;
                    case 3:
                        imageView.setImageResource(R.drawable.car_video_p4);
                        dialogPicture.setContentView(imageView);
                        windowPicture.setAttributes(lpPicture);
                        dialogPicture.show();
                        break;
                    case 4:
                        imageView.setImageResource(R.drawable.car_video_p5);
                        dialogPicture.setContentView(imageView);
                        windowPicture.setAttributes(lpPicture);
                        dialogPicture.show();
                        break;
                    case 5:
                        imageView.setImageResource(R.drawable.car_video_p6);
                        dialogPicture.setContentView(imageView);
                        windowPicture.setAttributes(lpPicture);
                        dialogPicture.show();
                        break;
                    case 6:
                        imageView.setImageResource(R.drawable.car_video_p7);
                        dialogPicture.setContentView(imageView);
                        windowPicture.setAttributes(lpPicture);
                        dialogPicture.show();
                        break;
                    case 7:
                        imageView.setImageResource(R.drawable.car_video_p8);
                        dialogPicture.setContentView(imageView);
                        windowPicture.setAttributes(lpPicture);
                        dialogPicture.show();
                        break;
                }
            }
        });
    }

    //GridView Video获取数据
    public List<Map<String, Object>> getData() {
        for (int i = 0; i < videoName.length; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", R.mipmap.video_icon);
            map.put("text", videoName[i]);
            data_list.add(map);
        }
        return data_list;
    }

    //编写图片适配器
    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        public ImageAdapter(Context c) {
            mContext = c;
        }
        @Override
        public int getCount() {
            return pictures.length;
        }
        @Override
        public Object getItem(int i) {
            return null;
        }
        @Override
        public long getItemId(int i) {
            return 0;
        }
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ImageView imageView;
            if (view == null) {
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(200, 200));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } else {
                imageView = (ImageView) view;
            }
            imageView.setImageResource(pictures[i]);
            return imageView;
        }
    }

    //设置布局文件
    @Override
    protected int getLayout() {
        return R.layout.activity_car_violation;
    }
}
