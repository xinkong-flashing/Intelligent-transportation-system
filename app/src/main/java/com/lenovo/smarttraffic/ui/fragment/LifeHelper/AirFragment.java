package com.lenovo.smarttraffic.ui.fragment.LifeHelper;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.database.dao.LifeHelperDatabase;

import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
public class AirFragment extends Fragment {


    private View view;
    private WebView webView;
    private LifeHelperDatabase lifeHelperDatabase;

    public AirFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_air, container, false);
        //创建数据库
        lifeHelperDatabase = new LifeHelperDatabase(getContext(), "LifeHelper.db", null, 1);
        lifeHelperDatabase.getWritableDatabase();
        //加载WebView
        webView = view.findViewById(R.id.life_helper_air_fragment_webview);
        webView.loadUrl("file:///android_asset/LifeHelper/BarGraph.html");
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        //handler每3秒发送一次消息
        handler.sendEmptyMessage(0x011);
        return view;
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0x011) {
                //获取数据库数据
                int n = 0;
                int[] data = new int[20];
                String[] time = new String[20];
                SQLiteDatabase db = lifeHelperDatabase.getWritableDatabase();
                Cursor cursor = db.query("AllWeather", null, null, null, null, null, null);
                if (cursor.moveToFirst()) {
                    do {
                        data[n] = cursor.getInt(cursor.getColumnIndex("pm25"));
                        time[n] = cursor.getString(cursor.getColumnIndex("time"));
                        n++;
                    }while (cursor.moveToNext());
                }
                cursor.close();
                //找出这20个数据中的空气质量最差值
                int maxData = 0;
                for (int i = 0; i < data.length; i++) {
                    if (data[i] > maxData) {
                        maxData = data[i];
                    }
                }
                TextView textView = view.findViewById(R.id.life_helper_air_fragment_textview);
                textView.setText("过去1分钟空气质量最差值："+maxData);
                //传给Web
                webView.loadUrl("javascript:getData(\""+ Arrays.toString(time) +"\", \""+Arrays.toString(data)+"\")");
            }
            handler.sendEmptyMessageDelayed(0x011, 3000);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
}
