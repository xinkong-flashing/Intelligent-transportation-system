package com.lenovo.smarttraffic.ui.activity.environment;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.database.dao.EnvironmentDatabase;
import com.lenovo.smarttraffic.ui.activity.BaseActivity;

import java.util.Arrays;

public class WayActivity extends BaseActivity {

    private WebView webView;
    private final int TEMPERATURE_MAG = 0x001;
    private EnvironmentDatabase environmentDatabase;
    private int[] data = new int[20];
    private String[] time = new String[20];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义标题栏
        initToolBar(findViewById(R.id.toolbar), true, "实时显示");

        //创建数据库
        environmentDatabase = new EnvironmentDatabase(this, "Environment.db", null, 1);
        environmentDatabase.getWritableDatabase();

        //加载WebView界面
        webView = findViewById(R.id.way_webview);
        //访问本地HTML文件
        webView.loadUrl("file:///android_asset/RealtimeDisplay.html");
        //不使用Android默认浏览器打开Web，就在App内打开
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        //支持App内部JavaScript交互
        webView.getSettings().setJavaScriptEnabled(true);

        //handler发送消息
        handler.sendEmptyMessage(TEMPERATURE_MAG);
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == TEMPERATURE_MAG) {
                int i = 0;
                SQLiteDatabase db = environmentDatabase.getWritableDatabase();
                //查询Target表中的数据
                Cursor cursor = db.query("Target", null, null, null, null, null, null);
                if (cursor.moveToFirst()) {
                    do {
                        data[i] = cursor.getInt(cursor.getColumnIndex("way"));
                        time[i] = cursor.getString(cursor.getColumnIndex("time"));
                        i++;
                    } while (cursor.moveToNext());
                }
                cursor.close();
                webView.loadUrl("javascript:setData('"+ Arrays.toString(data)+"')");
                webView.loadUrl("javascript:setTime('"+ Arrays.toString(time)+"')");
                handler.sendEmptyMessageDelayed(TEMPERATURE_MAG, 3000);
            }
        }
    };

    @Override
    protected int getLayout() {
        return R.layout.activity_way;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

}
