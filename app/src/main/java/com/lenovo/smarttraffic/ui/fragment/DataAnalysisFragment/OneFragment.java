package com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment;


import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.DataAnalysis.OneFragmentInfo;
import com.lenovo.smarttraffic.bean.ROWS_DETAIL;

import java.util.HashSet;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class OneFragment extends Fragment {


    private View view;
    private WebView webView;
    private List<OneFragmentInfo.ROWS_DETAIL> allList;

    public OneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_one, container, false);
        //初始化WebView
        webView = view.findViewById(R.id.data_analysis_webview1);
        webView.loadUrl("file:///android_asset/DataAnalysis/OneFragment.html");
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        //解析数据
        sendAllCar();
        return view;
    }

    //请求所有车辆的数据
    public void sendAllCar() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient();
                    RequestBody requestBody = RequestBody.create(MediaType.parse("app/json"),
                            "{\"UserName\":\"user1\"}");
                    Request request = new Request.Builder()
                            .url("http://192.168.1.102:8088/transportservice/action/GetCarInfo.do")
                            .post(requestBody)
                            .build();
                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    //数据解析
                    parseAllCar(responseData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void parseAllCar(String jsonData) {
        Gson gson = new Gson();
        OneFragmentInfo info = gson.fromJson(jsonData, OneFragmentInfo.class);
        allList = info.getROWS_DETAIL();
        //请求有违章记录的车辆
        sendWeizhanghCar();
    }

    //请求有违章记录的车辆
    public void sendWeizhanghCar() {
        try {
            OkHttpClient client = new OkHttpClient();
            RequestBody requestBody = RequestBody.create(MediaType.parse("app/json"),
                    "{\"UserName\":\"user1\"}");
            Request request = new Request.Builder()
                    .url("http://192.168.1.102:8088/transportservice/action/GetAllCarPeccancy.do")
                    .post(requestBody)
                    .build();
            Response response = client.newCall(request).execute();
            String responseData = response.body().string();
            //数据解析
            parseWeizhangCar(responseData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void parseWeizhangCar(String jsonData) {
        Gson gson = null;
        try {
            gson = new Gson();
        } catch (Exception e) {
            e.printStackTrace();
        }
        OneFragmentInfo info = gson.fromJson(jsonData, OneFragmentInfo.class);
        List<OneFragmentInfo.ROWS_DETAIL> weizhangList = info.getROWS_DETAIL();
        //给集合去重
        for (int i = 0; i < weizhangList.size() - 1; i++) {
            for (int j = i+1; j < weizhangList.size(); j++) {
                if ((weizhangList.get(i).getCarnumber()).equals(weizhangList.get(j).getCarnumber())) {
                    weizhangList.remove(j);
                    j--;
                }
            }
        }
        //给WebView传递参数进行绘图
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int yes = weizhangList.size();
                int no = allList.size() - yes;
                webView.loadUrl("javascript:getData(\""+yes+"\", \""+no+"\")");
            }
        });
    }

}
