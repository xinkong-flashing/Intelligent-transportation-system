package com.lenovo.smarttraffic.ui.activity;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment.FiveFragment;
import com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment.ForeFragment;
import com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment.OneFragment;
import com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment.SevenFragment;
import com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment.SixFragment;
import com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment.ThreeFragment;
import com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment.TwoFragment;

import java.util.ArrayList;
import java.util.List;

public class DataAnalysisActivity extends BaseActivity {

    private TextView tv1;
    private TextView tv2;
    private TextView tv3;
    private TextView tv4;
    private TextView tv5;
    private TextView tv6;
    private TextView tv7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义导航栏
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item12));
        //配置ViewPager
        initView();
    }

    //配置ViewPager的小圆点的点击事件和配置ViewPager的Fragment集合
    public void initView() {
        tv1 = findViewById(R.id.data_analysis_viewpager_tv1);
        tv2 = findViewById(R.id.data_analysis_viewpager_tv2);
        tv3 = findViewById(R.id.data_analysis_viewpager_tv3);
        tv4 = findViewById(R.id.data_analysis_viewpager_tv4);
        tv5 = findViewById(R.id.data_analysis_viewpager_tv5);
        tv6 = findViewById(R.id.data_analysis_viewpager_tv6);
        tv7 = findViewById(R.id.data_analysis_viewpager_tv7);
        ViewPager viewPager = findViewById(R.id.data_analysis_viewpager);
        //绑定小圆点的监听事件
        viewPager.setOnPageChangeListener(new MyPagerChangeListener());
        //把Fragment添加到List集合里面
        List<Fragment> list = new ArrayList<>();
        list.add(new OneFragment());
        list.add(new TwoFragment());
        list.add(new ThreeFragment());
        list.add(new ForeFragment());
        list.add(new FiveFragment());
        list.add(new SixFragment());
        list.add(new SevenFragment());
        DataAnalysisViewPagerAdapter adapter = new DataAnalysisViewPagerAdapter(getSupportFragmentManager(), list);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);    //初始显示第一个页面（从0开始计数）
    }

    /**
     * 设置一个ViewPager的监听事件，当左右滑动ViewPager时，底部小圆点改变状态
     */
    public class MyPagerChangeListener implements ViewPager.OnPageChangeListener{

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    tv1.setBackgroundResource(R.drawable.viewpager_tv_gray);
                    tv2.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv3.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv4.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv5.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv6.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv7.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    break;
                case 1:
                    tv1.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv2.setBackgroundResource(R.drawable.viewpager_tv_gray);
                    tv3.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv4.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv5.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv6.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv7.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    break;
                case 2:
                    tv1.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv2.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv3.setBackgroundResource(R.drawable.viewpager_tv_gray);
                    tv4.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv5.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv6.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv7.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    break;
                case 3:
                    tv1.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv2.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv3.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv4.setBackgroundResource(R.drawable.viewpager_tv_gray);
                    tv5.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv6.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv7.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    break;
                case 4:
                    tv1.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv2.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv3.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv4.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv5.setBackgroundResource(R.drawable.viewpager_tv_gray);
                    tv6.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv7.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    break;
                case 5:
                    tv1.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv2.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv3.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv4.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv5.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv6.setBackgroundResource(R.drawable.viewpager_tv_gray);
                    tv7.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    break;
                case 6:
                    tv1.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv2.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv3.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv4.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv5.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv6.setBackgroundResource(R.drawable.viewpager_tv_wt);
                    tv7.setBackgroundResource(R.drawable.viewpager_tv_gray);
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_data_analysis;
    }
}
