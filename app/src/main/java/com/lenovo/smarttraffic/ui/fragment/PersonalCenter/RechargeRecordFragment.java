package com.lenovo.smarttraffic.ui.fragment.PersonalCenter;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.database.dao.AccountManagementDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RechargeRecordFragment extends Fragment {


    private View view;
    private AccountManagementDatabase accountManagementDatabase;
    private List<String[]> list;

    public RechargeRecordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_recharge_record2, container, false);
        //创建数据库
        accountManagementDatabase = new AccountManagementDatabase(getActivity(), "AccountManage.db", null, 1);
        accountManagementDatabase.getWritableDatabase();
        //获取数据库的数据
        getData();
        //倒序List集合
        Collections.reverse(list);
        //获取总支出
        getAllExpend();
        //加载进ListView
        ListView listView = view.findViewById(R.id.recharge_record_fragment_listview);
        MyListViewAdapter adapter = new MyListViewAdapter();
        listView.setAdapter(adapter);
        return view;
    }

    //获取总支出
    public void getAllExpend() {
        int expend = 0;
        for (int i = 0; i < list.size(); i++) {
            expend = expend + Integer.parseInt(list.get(i)[2]);
        }
        TextView textView = view.findViewById(R.id.recharge_record_fragment_allexpend);
        textView.setText(expend+"");
    }

    //获取数据库的数据
    public void getData() {
        list = new ArrayList<>();
        SQLiteDatabase db = accountManagementDatabase.getWritableDatabase();
        Cursor cursor = db.query("Account", null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                if (cursor.getInt(cursor.getColumnIndex("id")) > 4){
                    String[] data = new String[5];
                    data[0] = cursor.getString(cursor.getColumnIndex("people"));
                    data[1] = cursor.getString(cursor.getColumnIndex("carnumber"));
                    data[2] = cursor.getInt(cursor.getColumnIndex("recharge"))+"";
                    data[3] = cursor.getInt(cursor.getColumnIndex("balance"))+"";
                    data[4] = cursor.getString(cursor.getColumnIndex("time"));
                    list.add(data);
                }
            }while (cursor.moveToNext());
        }
        cursor.close();
    }

    //ListView适配器
    private class MyListViewAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = View.inflate(getActivity(), R.layout.recharge_recode_listview_item, null);
            TextView tvPeople = view1.findViewById(R.id.recharge_record_fragment_item_people);
            TextView tvCarnumber = view1.findViewById(R.id.recharge_record_fragment_item_carnumber);
            TextView tvRecharge = view1.findViewById(R.id.recharge_record_fragment_item_recharge);
            TextView tvBalance = view1.findViewById(R.id.recharge_record_fragment_item_balance);
            TextView tvTime = view1.findViewById(R.id.recharge_record_fragment_item_time);
            tvPeople.setText("充值人："+list.get(i)[0]);
            tvCarnumber.setText("车牌号："+list.get(i)[1]);
            tvRecharge.setText("充值："+list.get(i)[2]);
            tvBalance.setText("余额："+list.get(i)[3]);
            tvTime.setText(list.get(i)[4]);
            return view1;
        }
    }

}
