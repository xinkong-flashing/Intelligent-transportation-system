package com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.DataAnalysis.OneFragmentInfo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class TwoFragment extends Fragment {


    private View view;
    private WebView webView;
    private int count;
    private int total;

    public TwoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_two, container, false);
        //初始化webview
        webView = view.findViewById(R.id.data_analysis_webview2);
        webView.loadUrl("file:///android_asset/DataAnalysis/TwoFragment.html");
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        //违章车辆数据请求
        sendCarData();
        return view;
    }

    //违章车辆数据请求
    public void sendCarData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    OkHttpClient client = new OkHttpClient();
                    RequestBody requestBody = RequestBody.create(MediaType.parse("app/json"),
                            "{\"UserName\":\"user1\"}");
                    Request request = new Request.Builder()
                            .url("http://192.168.1.102:8088/transportservice/action/GetAllCarPeccancy.do")
                            .post(requestBody)
                            .build();
                    Response response = client.newCall(request).execute();
                    String parseData = response.body().string();
                    //数据解析
                    parseData(parseData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void parseData(String jsonData) {
        Gson gson = new Gson();
        OneFragmentInfo info = gson.fromJson(jsonData, OneFragmentInfo.class);
        List<OneFragmentInfo.ROWS_DETAIL> list = info.getROWS_DETAIL();
        //去掉重复的违章车辆
        Set<String> set = new HashSet();
        List<String> list1 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            set.add(list.get(i).getCarnumber());
            list1.add(list.get(i).getCarnumber());
        }
        //有重复违章的车辆
        count = 0;
        for (Object carnumber : set) {
            if (Collections.frequency(list1, carnumber) > 1) {
                count++;
            }
        }
        //没有重复违章的车辆
        total = set.size() - count;
        //传递值给webview
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:getData(\""+count+"\", \""+total+"\")");
            }
        });
    }

}
