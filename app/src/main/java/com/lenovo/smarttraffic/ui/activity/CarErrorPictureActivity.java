package com.lenovo.smarttraffic.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;

import com.lenovo.smarttraffic.R;

public class CarErrorPictureActivity extends BaseActivity {

    public static int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义标题栏
        initToolBar(findViewById(R.id.toolbar), true, "监控抓拍");
        //返回按钮点击事件
        Button button = findViewById(R.id.weizhang_back);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CarErrorResultActivity.class);
                startActivity(intent);
            }
        });
        //初始化图片控件
        ImageView imageView1 = findViewById(R.id.weizhang1);
        ImageView imageView2 = findViewById(R.id.weizhang2);
        ImageView imageView3 = findViewById(R.id.weizhang3);
        ImageView imageView4 = findViewById(R.id.weizhang4);
        //图片的点击事件
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = 0;
                show();
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = 1;
                show();
            }
        });
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = 2;
                show();
            }
        });
        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = 3;
                show();
            }
        });
    }

    public void show(){
        Intent intent = new Intent(getApplicationContext(), CarErrorPicturesActivity.class);
        startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_car_error_picture;
    }
}
