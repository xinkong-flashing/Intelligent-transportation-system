package com.lenovo.smarttraffic.ui.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.database.dao.AccountManagementDatabase;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AccountManagementActivity extends BaseActivity implements View.OnClickListener{

    private AccountManagementDatabase accountManagementDatabase;
    private TextView tvBalance1;
    private TextView tvBalance2;
    private TextView tvBalance3;
    private TextView tvBalance4;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    int balance1 = 0;
    int balance2 = 0;
    int balance3 = 0;
    int balance4 = 0;
    private TextView tvPLCZ;
    private CheckBox checkBox1;
    private CheckBox checkBox2;
    private CheckBox checkBox3;
    private CheckBox checkBox4;
    private TextView tvCZJL;
    private LinearLayout layout1;
    private LinearLayout layout2;
    private LinearLayout layout3;
    private LinearLayout layout4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义标题栏
        initToolBar(findViewById(R.id.toolbar), true , getString(R.string.item14));
        //初始化控件
        init();
        //创建数据库
        accountManagementDatabase = new AccountManagementDatabase(this, "AccountManage.db", null, 1);
        accountManagementDatabase.getWritableDatabase();
        //若数据库为空，就添加初始数据
        isDatabaseNull();
        //显示余额信息
        showBalance();
        //判断余额是否低于阈值
        isDownYu();
    }

    //判断余额是否低于阈值
    public void isDownYu() {
        SharedPreferences pref = getSharedPreferences("data", MODE_PRIVATE);
        int data = pref.getInt("data", -1);
        if (data >= 0) {
            if (balance1 < data){
                layout1.setBackgroundColor(Color.YELLOW);
            } else {
                layout1.setBackgroundColor(Color.WHITE);
            }
            if (balance2 < data){
                layout2.setBackgroundColor(Color.YELLOW);
            } else {
                layout2.setBackgroundColor(Color.WHITE);
            }
            if (balance3 < data){
                layout3.setBackgroundColor(Color.YELLOW);
            } else {
                layout3.setBackgroundColor(Color.WHITE);
            }
            if (balance4 < data){
                layout4.setBackgroundColor(Color.YELLOW);
            } else {
                layout4.setBackgroundColor(Color.WHITE);
            }
        }
    }

    //判断checkbox被选中的个数
    public int getCheckBoxCount() {
        int i = 0;
        if (checkBox1.isChecked()) {
            i++;
        }
        if (checkBox2.isChecked()) {
            i++;
        }
        if (checkBox3.isChecked()) {
            i++;
        }
        if (checkBox4.isChecked()) {
            i++;
        }
        return i;
    }

    //自定义Dialog消息弹框
    private void curtomDialog(String carnumber, String people, int balance) {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        View view = LayoutInflater.from(this.getBaseContext()).inflate(R.layout.account_management_curtom_dialog, null, false);
        dialog.setView(view);
        dialog.setCanceledOnTouchOutside(false);    //点击外部dialog不消失
        dialog.show();
        TextView textView = view.findViewById(R.id.account_management_curtom_dialog_textview);
        EditText editText = view.findViewById(R.id.account_management_curtom_dialog_editview);
        textView.setText(carnumber);
        //dialog的点击事件
        Button btnCZ = view.findViewById(R.id.account_management_curtom_dialog_btncz);
        Button btnQX = view.findViewById(R.id.account_management_curtom_dialog_btnqx);
        btnCZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().equals("0") || editText.getText().toString().equals("")) {
                    Toast.makeText(AccountManagementActivity.this, "请输入合法的充值金额！", Toast.LENGTH_SHORT).show();
                } else {
                    //获取时间
                    SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd　HH:mm:ss");
                    String time = df.format(new Date());
                    //存储数据
                    SQLiteDatabase db = accountManagementDatabase.getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put("carnumber", carnumber);
                    values.put("recharge", Integer.parseInt(editText.getText().toString()));
                    values.put("balance", balance + Integer.parseInt(editText.getText().toString()));
                    values.put("people", people);
                    values.put("time", time);
                    db.insert("Account", null, values);
                    Toast.makeText(AccountManagementActivity.this, "充值成功！", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    //重新加载页面
                    showBalance();
                }
            }
        });
        btnQX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AccountManagementActivity.this, "取消充值！", Toast.LENGTH_SHORT).show();
                dialog.dismiss();   //关闭dialog
            }
        });
    }
    //自定义Dialog消息弹框
    //批量充值
    private void curtomDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        View view = LayoutInflater.from(this.getBaseContext()).inflate(R.layout.account_management_curtom_dialog, null, false);
        dialog.setView(view);
        dialog.setCanceledOnTouchOutside(false);    //点击外部dialog不消失
        dialog.show();
        TextView textView = view.findViewById(R.id.account_management_curtom_dialog_textview);
        EditText editText = view.findViewById(R.id.account_management_curtom_dialog_editview);
        String carnumber = "";
        if (checkBox1.isChecked()) {
            carnumber = carnumber + "辽A10001　";
        }
        if (checkBox2.isChecked()) {
            carnumber = carnumber + "辽A10002　";
        }
        if (checkBox3.isChecked()) {
            carnumber = carnumber + "辽A10003　";
        }
        if (checkBox4.isChecked()) {
            carnumber = carnumber + "辽A10004　";
        }
        textView.setText(carnumber);
        //dialog的点击事件
        Button btnCZ = view.findViewById(R.id.account_management_curtom_dialog_btncz);
        Button btnQX = view.findViewById(R.id.account_management_curtom_dialog_btnqx);
        btnCZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().equals("0") || editText.getText().toString().equals("")) {
                    Toast.makeText(AccountManagementActivity.this, "请输入合法的充值金额！", Toast.LENGTH_SHORT).show();
                } else {
                    //获取时间
                    SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd　HH:mm:ss");
                    String time = df.format(new Date());
                    //存储数据
                    SQLiteDatabase db = accountManagementDatabase.getWritableDatabase();
                    ContentValues values = new ContentValues();
                    if (checkBox1.isChecked()){
                        values.put("carnumber", "辽A10001");
                        values.put("recharge", Integer.parseInt(editText.getText().toString()));
                        values.put("balance", balance1 + Integer.parseInt(editText.getText().toString()));
                        values.put("people", "张三");
                        values.put("time", time);
                        db.insert("Account", null, values);
                        values.clear();
                    }
                    if (checkBox2.isChecked()){
                        values.put("carnumber", "辽A10002");
                        values.put("recharge", Integer.parseInt(editText.getText().toString()));
                        values.put("balance", balance2 + Integer.parseInt(editText.getText().toString()));
                        values.put("people", "李四");
                        values.put("time", time);
                        db.insert("Account", null, values);
                        values.clear();
                    }
                    if (checkBox3.isChecked()){
                        values.put("carnumber", "辽A10003");
                        values.put("recharge", Integer.parseInt(editText.getText().toString()));
                        values.put("balance", balance3 + Integer.parseInt(editText.getText().toString()));
                        values.put("people", "王五");
                        values.put("time", time);
                        db.insert("Account", null, values);
                        values.clear();
                    }
                    if (checkBox4.isChecked()){
                        values.put("carnumber", "辽A10004");
                        values.put("recharge", Integer.parseInt(editText.getText().toString()));
                        values.put("balance", balance4 + Integer.parseInt(editText.getText().toString()));
                        values.put("people", "赵六");
                        values.put("time", time);
                        db.insert("Account", null, values);
                        values.clear();
                    }
                    Toast.makeText(AccountManagementActivity.this, "充值成功！", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    //重新加载页面
                    showBalance();
                }
            }
        });
        btnQX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AccountManagementActivity.this, "取消充值！", Toast.LENGTH_SHORT).show();
                dialog.dismiss();   //关闭dialog
            }
        });
    }

    //显示余额信息
    public void showBalance() {
        SQLiteDatabase db = accountManagementDatabase.getWritableDatabase();
        Cursor cursor = db.query("Account", null, null, null, null, null, null);
        if (cursor.moveToNext()) {
            do {
                if (cursor.getString(cursor.getColumnIndex("carnumber")).equals("辽A10001")) {
                    balance1 = cursor.getInt(cursor.getColumnIndex("balance"));
                } else if (cursor.getString(cursor.getColumnIndex("carnumber")).equals("辽A10002")) {
                    balance2 = cursor.getInt(cursor.getColumnIndex("balance"));
                } else if (cursor.getString(cursor.getColumnIndex("carnumber")).equals("辽A10003")) {
                    balance3 = cursor.getInt(cursor.getColumnIndex("balance"));
                } else if (cursor.getString(cursor.getColumnIndex("carnumber")).equals("辽A10004")) {
                    balance4 = cursor.getInt(cursor.getColumnIndex("balance"));
                }
            }while (cursor.moveToNext());
        }
        cursor.close();
        tvBalance1.setText("余额："+balance1+"元");
        tvBalance2.setText("余额："+balance2+"元");
        tvBalance3.setText("余额："+balance3+"元");
        tvBalance4.setText("余额："+balance4+"元");
        isDownYu();
    }

    //若数据库为空，就添加初始值
    public void isDatabaseNull() {
        SQLiteDatabase db = accountManagementDatabase.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from Account", null);
        if (cursor.getCount() == 0) {
            ContentValues values = new ContentValues();
            values.put("carnumber", "辽A10001");
            values.put("recharge", 100);
            values.put("balance", 100);
            db.insert("Account", null, values);
            values.clear();
            values.put("carnumber", "辽A10002");
            values.put("recharge", 99);
            values.put("balance", 99);
            db.insert("Account", null, values);
            values.clear();
            values.put("carnumber", "辽A10003");
            values.put("recharge", 103);
            values.put("balance", 103);
            db.insert("Account", null, values);
            values.clear();
            values.put("carnumber", "辽A10004");
            values.put("recharge", 1);
            values.put("balance", 1);
            db.insert("Account", null, values);
        }
    }

    //初始化控件
    public void init() {
        tvBalance1 = findViewById(R.id.account_management_balance1);
        tvBalance2 = findViewById(R.id.account_management_balance2);
        tvBalance3 = findViewById(R.id.account_management_balance3);
        tvBalance4 = findViewById(R.id.account_management_balance4);
        button1 = findViewById(R.id.account_management_button1);
        button2 = findViewById(R.id.account_management_button2);
        button3 = findViewById(R.id.account_management_button3);
        button4 = findViewById(R.id.account_management_button4);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        tvPLCZ = findViewById(R.id.account_management_plcz);
        tvPLCZ.setOnClickListener(this);
        tvCZJL = findViewById(R.id.account_management_czjl);
        tvCZJL.setOnClickListener(this);
        checkBox1 = findViewById(R.id.account_management_checkbox1);
        checkBox2 = findViewById(R.id.account_management_checkbox2);
        checkBox3 = findViewById(R.id.account_management_checkbox3);
        checkBox4 = findViewById(R.id.account_management_checkbox4);
        layout1 = findViewById(R.id.account_management_layout1);
        layout2 = findViewById(R.id.account_management_layout2);
        layout3 = findViewById(R.id.account_management_layout3);
        layout4 = findViewById(R.id.account_management_layout4);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.account_management_button1:
                curtomDialog("辽A10001", "张三", balance1);
                break;
            case R.id.account_management_button2:
                curtomDialog("辽A10002", "李四", balance2);
                break;
            case R.id.account_management_button3:
                curtomDialog("辽A10003", "王五", balance3);
                break;
            case R.id.account_management_button4:
                curtomDialog("辽A10004", "赵六", balance4);
                break;
            case R.id.account_management_plcz:
                //判断checkbox被选中的个数
                if (getCheckBoxCount() < 2) {
                    Toast.makeText(this, "请选择 > 1 辆车进行批量充值！", Toast.LENGTH_SHORT).show();
                } else {
                    curtomDialog();
                }
                break;
            case R.id.account_management_czjl:
                Intent intent = new Intent(getApplicationContext(), PersonalCenterActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isDownYu();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_account_management;
    }

}
