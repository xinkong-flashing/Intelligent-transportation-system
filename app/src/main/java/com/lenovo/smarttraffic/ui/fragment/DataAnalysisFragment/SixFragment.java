package com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.DataAnalysis.ErrorCarMessageInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SixFragment extends Fragment {


    private View view;
    private List<ErrorCarMessageInfo.ROWS_DETAIL> list;
    private WebView webView;

    public SixFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_six, container, false);
        //初始化webview
        webView = view.findViewById(R.id.data_analysis_webview6);
        webView.loadUrl("file:///android_asset/DataAnalysis/SixFragment.html");
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        //请求违章数据
        sendErrorData();
        return view;
    }

    //请求违章数据
    public void sendErrorData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient();
                    RequestBody requestBody = RequestBody.create(MediaType.parse("app/json"),
                            "{\"UserName\":\"user1\"}");
                    Request request = new Request.Builder()
                            .url("http://192.168.1.102:8088/transportservice/action/GetAllCarPeccancy.do")
                            .post(requestBody)
                            .build();
                    Response response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    //解析数据
                    parseErrorData(responseData);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void parseErrorData(String jsonData) {
        Gson gson = new Gson();
        ErrorCarMessageInfo info = gson.fromJson(jsonData, ErrorCarMessageInfo.class);
        list = new ArrayList<>();
        list.addAll(info.getROWS_DETAIL());
        //区分日期为18位还是19位
        int[] timeData = new int[12];
        for (int i = 0; i < list.size(); i++) {
            String time = list.get(i).getPdatetime();
            if (time.length() == 18) {
                if (time.substring(10, 12).equals("12") || time.substring(10, 12).equals("01")) {
                    timeData[0]++;
                } else if (time.substring(10, 12).equals("02") || time.substring(10, 12).equals("03")) {
                    timeData[1]++;
                } else if (time.substring(10, 12).equals("04") || time.substring(10, 12).equals("05")) {
                    timeData[2]++;
                }else if (time.substring(10, 12).equals("06") || time.substring(10, 12).equals("07")) {
                    timeData[3]++;
                }else if (time.substring(10, 12).equals("08") || time.substring(10, 12).equals("09")) {
                    timeData[4]++;
                }else if (time.substring(10, 12).equals("10") || time.substring(10, 12).equals("11")) {
                    timeData[5]++;
                }else if (time.substring(10, 12).equals("13")) {
                    timeData[6]++;
                }else if (time.substring(10, 12).equals("14") || time.substring(10, 12).equals("15")) {
                    timeData[7]++;
                }else if (time.substring(10, 12).equals("16") || time.substring(10, 12).equals("17")) {
                    timeData[8]++;
                }else if (time.substring(10, 12).equals("18") || time.substring(10, 12).equals("19")) {
                    timeData[9]++;
                }else if (time.substring(10, 12).equals("20") || time.substring(10, 12).equals("21")) {
                    timeData[10]++;
                }else if (time.substring(10, 12).equals("22") || time.substring(10, 12).equals("23")) {
                    timeData[11]++;
                }
            }else {
                if (time.substring(11, 13).equals("12") || time.substring(11, 13).equals("01")) {
                    timeData[0]++;
                } else if (time.substring(11, 13).equals("02") || time.substring(11, 13).equals("03")) {
                    timeData[1]++;
                } else if (time.substring(11, 13).equals("04") || time.substring(11, 13).equals("05")) {
                    timeData[2]++;
                }else if (time.substring(11, 13).equals("06") || time.substring(11, 13).equals("07")) {
                    timeData[3]++;
                }else if (time.substring(11, 13).equals("08") || time.substring(11, 13).equals("09")) {
                    timeData[4]++;
                }else if (time.substring(11, 13).equals("10") || time.substring(11, 13).equals("11")) {
                    timeData[5]++;
                }else if (time.substring(11, 13).equals("13")) {
                    timeData[6]++;
                }else if (time.substring(11, 13).equals("14") || time.substring(11, 13).equals("15")) {
                    timeData[7]++;
                }else if (time.substring(11, 13).equals("16") || time.substring(11, 13).equals("17")) {
                    timeData[8]++;
                }else if (time.substring(11, 13).equals("18") || time.substring(11, 13).equals("19")) {
                    timeData[9]++;
                }else if (time.substring(11, 13).equals("20") || time.substring(11, 13).equals("21")) {
                    timeData[10]++;
                }else if (time.substring(11, 13).equals("22") || time.substring(11, 13).equals("23")) {
                    timeData[11]++;
                }
            }
        }

        //给webview传递数组数据
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:getData(\""+ Arrays.toString(timeData) +"\")");
            }
        });

    }

}
