package com.lenovo.smarttraffic.ui.fragment.DataAnalysisFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.DataAnalysis.OneFragmentInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThreeFragment extends Fragment {


    private View view;
    private int one;
    private int two;
    private int three;
    private WebView webView;

    public ThreeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_three, container, false);
        //初始化WebView
        webView = view.findViewById(R.id.data_analysis_webview3);
        webView.loadUrl("file:///android_asset/DataAnalysis/ThreeFragment.html");
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        //请求所有的违章车辆
        sendCarData();
        return view;
    }

    //请求所有的违章车辆
    public void sendCarData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient();
                    RequestBody requestBody  = RequestBody.create(MediaType.parse("app/json"),
                            "{\"UserName\":\"user1\"}");
                    Request request = new Request.Builder()
                            .url("http://192.168.1.102:8088/transportservice/action/GetAllCarPeccancy.do")
                            .post(requestBody)
                            .build();
                    Response response = client.newCall(request).execute();
                    String parseData = response.body().string();
                    //解析数据
                    parseCarData(parseData);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    public void parseCarData(String jsonData){
        Gson gson = new Gson();
        OneFragmentInfo info = gson.fromJson(jsonData, OneFragmentInfo.class);
        Set set = new HashSet();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < info.getROWS_DETAIL().size(); i++) {
            set.add(info.getROWS_DETAIL().get(i).getCarnumber());
            list.add(info.getROWS_DETAIL().get(i).getCarnumber());
        }
        //获取违章数目的条数
        one = 0;
        two = 0;
        three = 0;
        for (Object carnumber : set) {
            if (Collections.frequency(list, carnumber) > 0 && Collections.frequency(list, carnumber) < 3) {
                one++;
            } else if (Collections.frequency(list, carnumber) > 2 && Collections.frequency(list, carnumber) < 6) {
                two++;
            }else {
                three++;
            }
        }
        //给WebView传递数据
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:getData(\""+one+"\", \""+two+"\", \""+three+"\")");
            }
        });
    }

}
