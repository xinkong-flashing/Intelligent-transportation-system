package com.lenovo.smarttraffic.ui.activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.lenovo.smarttraffic.R;

public class ThresholdValueSetActivity extends BaseActivity {

    private Switch thresholdSwitch;
    private TextView tvSwitch;
    private EditText etTem;
    private EditText etHum;
    private EditText etIll;
    private EditText etCo;
    private EditText etPm;
    private EditText etWay;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //自定义标题栏
        initToolBar(findViewById(R.id.toolbar), true, getString(R.string.item7));

        //定义控件id
        thresholdSwitch = findViewById(R.id.threshold_switch);
        tvSwitch = findViewById(R.id.tv_switch);
        etTem = findViewById(R.id.threshold_et_tem);
        etHum = findViewById(R.id.threshold_et_hum);
        etIll = findViewById(R.id.threshold_et_ill);
        etCo = findViewById(R.id.threshold_et_co);
        etPm = findViewById(R.id.threshold_et_pm);
        etWay = findViewById(R.id.threshold_et_way);
        button = findViewById(R.id.threshold_button);

        //使EditView初始时不可编辑
        etTem.setFocusable(false);
        etTem.setFocusableInTouchMode(false);//不可编辑
        etHum.setFocusable(false);
        etHum.setFocusableInTouchMode(false);//不可编辑
        etIll.setFocusable(false);
        etIll.setFocusableInTouchMode(false);//不可编辑
        etCo.setFocusable(false);
        etCo.setFocusableInTouchMode(false);//不可编辑
        etPm.setFocusable(false);
        etPm.setFocusableInTouchMode(false);//不可编辑
        etWay.setFocusable(false);
        etWay.setFocusableInTouchMode(false);//不可编辑

        //switch开关监听器
        thresholdSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {    //如果为开时
                    //更改开关文字
                   tvSwitch.setText("开");
                   //EditView控件限制使用
                    etTem.setFocusable(false);
                    etTem.setFocusableInTouchMode(false);//不可编辑
                    etHum.setFocusable(false);
                    etHum.setFocusableInTouchMode(false);//不可编辑
                    etIll.setFocusable(false);
                    etIll.setFocusableInTouchMode(false);//不可编辑
                    etCo.setFocusable(false);
                    etCo.setFocusableInTouchMode(false);//不可编辑
                    etPm.setFocusable(false);
                    etPm.setFocusableInTouchMode(false);//不可编辑
                    etWay.setFocusable(false);
                    etWay.setFocusableInTouchMode(false);//不可编辑
                } else {        //为关时
                    //更改开关文字
                    tvSwitch.setText("关");
                    //设置EditView为可编辑状态
                    etTem.setFocusableInTouchMode(true);
                    etTem.setFocusable(true);
                    etTem.requestFocus();
                    etHum.setFocusableInTouchMode(true);
                    etHum.setFocusable(true);
                    etHum.requestFocus();
                    etIll.setFocusableInTouchMode(true);
                    etIll.setFocusable(true);
                    etIll.requestFocus();
                    etCo.setFocusableInTouchMode(true);
                    etCo.setFocusable(true);
                    etCo.requestFocus();
                    etPm.setFocusableInTouchMode(true);
                    etPm.setFocusable(true);
                    etPm.requestFocus();
                    etWay.setFocusableInTouchMode(true);
                    etWay.setFocusable(true);
                    etWay.requestFocus();
                }
            }
        });

        //button按钮监听器
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foundSp();
            }
        });

        //判断SharedPreferences是否创建
        SharedPreferences pref = getSharedPreferences("data", MODE_PRIVATE);
        int tem = pref.getInt("tem", 0);
        int hum = pref.getInt("hum", 0);
        int ill = pref.getInt("ill", 0);
        int co = pref.getInt("co", 0);
        int pm = pref.getInt("pm", 0);
        int way = pref.getInt("way", 0);
        if (tem == 0 && hum == 0 && ill == 0 && co == 0 && pm == 0 && way == 0) {
        } else {
            etTem.setText(tem+"");
            etHum.setText(hum+"");
            etIll.setText(ill+"");
            etCo.setText(co+"");
            etPm.setText(pm+"");
            etWay.setText(way+"");
        }

    }

    //创建SharedPreferences存储
    public void foundSp() {
        SharedPreferences.Editor editor = getSharedPreferences("data", MODE_PRIVATE).edit();
        editor.putInt("tem", Integer.parseInt(etTem.getText().toString()));
        editor.putInt("hum", Integer.parseInt(etHum.getText().toString()));
        editor.putInt("ill", Integer.parseInt(etIll.getText().toString()));
        editor.putInt("co", Integer.parseInt(etCo.getText().toString()));
        editor.putInt("pm", Integer.parseInt(etPm.getText().toString()));
        editor.putInt("way", Integer.parseInt(etWay.getText().toString()));
        editor.apply();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_threshold_value_set;
    }
}
