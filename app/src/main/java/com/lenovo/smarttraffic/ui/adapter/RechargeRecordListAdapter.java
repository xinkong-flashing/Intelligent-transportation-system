package com.lenovo.smarttraffic.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lenovo.smarttraffic.R;
import com.lenovo.smarttraffic.bean.RechargeRecordInfo;

import java.util.List;

public class RechargeRecordListAdapter extends BaseAdapter {

    private Context context;
    private List<RechargeRecordInfo> list;

    public RechargeRecordListAdapter(Context context, List<RechargeRecordInfo> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder viewHolder;
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.recharge_record_list_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.plate = view.findViewById(R.id.recharge_record_plate);
            viewHolder.money = view.findViewById(R.id.recharge_record_money);
            viewHolder.balance = view.findViewById(R.id.recharge_record_balance);
            viewHolder.people = view.findViewById(R.id.recharge_record_people);
            viewHolder.time = view.findViewById(R.id.recharge_record_time);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ViewHolder) view.getTag();
        }
        if (position == 0) {
            viewHolder.plate.setText("车牌号");
            viewHolder.money.setText("充值金额");
            viewHolder.balance.setText("充值后余额");
            viewHolder.people.setText("充值人");
            viewHolder.time.setText("充值时间");
        } else {
            viewHolder.plate.setText(list.get(position).getPlate());
            viewHolder.money.setText(list.get(position).getMoney()+"");
            viewHolder.balance.setText(list.get(position).getBalance()+"");
            viewHolder.people.setText(list.get(position).getPeople());
            viewHolder.time.setText(list.get(position).getTime());
        }
        return view;
    }
    class ViewHolder {
        TextView plate;
        TextView money;
        TextView balance;
        TextView people;
        TextView time;
    }
}
